'use strict'
import fs from 'fs'
import _  from 'lodash'
import path from 'path'
import ms from 'milliseconds'
import { errorObj, logger, AuthVerify } from './func-helpers'
import {
  AuthenticationError,
} from 'apollo-server-express'


export default class AuthHelpers {
  constructor () {
    logger.log('Loaded Auth Check')
    this.nonAuthPaths = []
    this.expiresIn = ms.weeks(2)
    this.TokenData = null
  }
  
  async JwtVerify ({ req }) {
    try {
      // check header or url parameters or post parameters for token
      const AuthToken = req.body.token || req.query.token || req.headers['x-access-token']
  
      const filtered = _.filter(this.nonAuthPaths, (obj) => {
        return req.body.operationName.indexOf(obj) !== -1
      })
  
      // Bypass Auth Token
      if (_.size(filtered) === 1 && !token) return
      // decode token
      if (AuthToken) {
        const {err, token} = await AuthVerify(AuthToken)
        
        if (token) {
          return token
        }
  
        if (err) {
          throw new AuthenticationError(
            'Failed to authenticate token.'
          )
        }
  
      } else {
        throw new AuthenticationError(
          'No token provided.'
        )
      }
    } catch (e) {
      return new Error(e)
    }
  }
  
  isAuthenticated ({ req, res, next }) {
    if (req.isAuthenticated()) {
      return next()
    }
  
    res.status(401).redirect('/account/login')
  }
}
