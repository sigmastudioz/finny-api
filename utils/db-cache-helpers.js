import _ from 'lodash'
import { errorObj, logger } from './func-helpers'

export default class DBCacheHelper {
  constructor () {
    this.cache = null
    this.db = null
    this.Model = null
    this.ModelData = null
    this.searchQuery = {}
    this.cacheKey = null
    this.dbUrl = null
  }

  async getSingle ({ Model = null, searchQuery = {}, cacheKey = null}) {
    try {
      let db = this.db
      const cache = this.cache
      let findModel = null
      if (cacheKey) {
        let cacheResults = await cache.getCache({
          cacheKey
        })
        if (cacheResults.status) {
          return _.merge(cacheResults.data, {
            status: cacheResults.status,
            key: cacheResults.key
          })
        } else {
          findModel = await db.findModel({
            Model,
            query: searchQuery.query
          })
          const results = findModel.data
          if (findModel.status) {
            cacheResults = await cache.setCache({
              cacheKey,
              cacheData: results
            })

            return _.merge(results, {
              status: findModel.status,
              key: cacheResults.key
            })
          } else {
            return _.merge(results, {
              status: findModel.status
            })
          }
        }
      } else {
        findModel = await db.findModel({
          Model,
          query: searchQuery.query
        })
        
        return _.merge(findModel.data, {
          status: findModel.status
        })
      }
     
    } catch (e) {
      return new Error(e)
    }
  }

  async getCollection ({ Model = null, searchQuery = {}, cacheKey = null}) {
    try {
      const cache = this.cache
      const db = this.db
      
      if (cacheKey) {
        let cacheResults = await cache.getCache({
          cacheKey
        })
        if (cacheResults.status) {
          return _.merge(cacheResults.data, {
            key: cacheResults.key
          })
        } else {
          const findAllModel = await db.findAllModel({
            Model,
            query: searchQuery
          })
          if (findAllModel.status) {
            cacheResults = await cache.setCache({
              cacheKey,
              cacheData: findAllModel
            })
            return _.merge(cacheResults.data, {
              key: cacheResults.key
            })
          } else {
            return findAllModel
          }
        }
      } else {
        return await db.findAllModel({
          Model,
          query: searchQuery
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async getCollectionAggregate ({ Model = null, searchQuery = {}, cacheKey = null}) {
    try {
      const cache = this.cache
      const db = this.db
      
      if (cacheKey) {
        let cacheResults = await cache.getCache({
          cacheKey
        })
        if (cacheResults.status) {
          return _.merge(cacheResults.data, {
            key: cacheResults.key
          })
        } else {
          const findAllModelAggregate = await db.findAllModelAggregate({
            Model,
            query: searchQuery
          })
          if (findAllModelAggregate.status) {
            cacheResults = await cache.setCache({
              cacheKey,
              cacheData: findAllModelAggregate
            })
            return _.merge(cacheResults.data, {
              key: cacheResults.key
            })
          } else {
            return findAllModelAggregate
          }
        }
      } else {
        return await db.findAllModelAggregate({
          Model,
          query: searchQuery
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }


  async createSingle ({ Model = null, ModelData = {} }) {
    try {
      const db = this.db
      const createModel = await db.createModel({ Model, ModelData })
      if (createModel.status) {
        return createModel.data
      } else {
        return createModel
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async updateSingle ({ Model = null, searchQuery = {}, ModelData = {}, cacheKey = null }) {
    try {
      const cache = this.cache
      const db = this.db

      if (_.has(ModelData, '_id')) {
        delete ModelData._id
      }

      const updateSingle = await db.updateSingle({ Model, ModelData, searchQuery})
      if (cacheKey && updateSingle.status) {
        const cacheResults = await cache.refreshCache({
          cacheKey,
          cacheData: updateSingle.data
        })
        if (cacheResults.status) {
          return cacheResults.data
        } else {
          return cacheResults
        }
      } else {
        return updateSingle
      }

    } catch (e) {
      return new Error(e)
    }
  }


  async deleteSingle ({ Model = null, searchQuery = {}, cacheKey = null }) {
    try {
      const cache = this.cache
      const db = this.db

      db.searchQuery = {
        _id: searchQuery._id
      }
      
      const deleteSingle = await db.deleteSingle({ Model, searchQuery })
      if (cacheKey && deleteSingle.status) {
        cache.delCache({
          cacheKey
        })
      }
      return deleteSingle.data
    } catch (e) {
      return new Error(e)
    }
  }

  async updateBulk ({ Model = null, searchQuery = {}, cacheKey = null, newData = {} }) {
    try {
      const cache = this.cache
      const db = this.db
      
      const bulkUpdate = await db.bulkUpdate({ Model, searchQuery, newData })
      if (bulkUpdate.status) {
        // const cacheResults = await cache.delCache({
        //   cacheKey
        // })
        // if (cacheResults.status) {
        //   return cacheResults.data
        // } else {
        //   return cacheResults
        // }
        return bulkUpdate
      } else {
        return bulkUpdate
      }
    } catch (e) {
      return new Error(e)
    }
  }


  async deleteBulk ({ Model = null, searchQuery = {}, cacheKey = null }) {
    try {
      const cache = this.cache
      const db = this.db
      
      const bulkDelete = await db.bulkDelete({ Model, searchQuery })
      if (bulkDelete.status) {
        // const cacheResults = await cache.delCache({
        //   cacheKey
        // })
        // if (cacheResults.status) {
        //   return cacheResults.data
        // } else {
        //   return cacheResults
        // }
        return bulkDelete
      } else {
        return bulkDelete
      }
    } catch (e) {
      return new Error(e)
    }
  }
}