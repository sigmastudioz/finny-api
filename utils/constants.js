const siteLogger = process.env.SITE_LOGGER
export const site_logger = (typeof siteLogger !== 'undefined')? (siteLogger === 'true') : true
export const stdTtl = process.env.CACHE_STDTTL || 0
export const cacheUrl = process.env.REDIS_URL || 'redis://localhost:6379'


// AWS S3
export const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID || null
export const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY || null
export const AWS_REGION = process.env.AWS_REGION || 'us-east-1'
export const AWS_BUCKET_NAME = process.env.AWS_BUCKET_NAME || null
export const AWS_BUCKET_KEY = process.env.AWS_BUCKET_KEY || null

// MONGODB
export const DATABASE_URL = process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/finny-app'

// ELASTIC SEARCH
export const ELASTIC_URL = process.env.ELASTIC_URL || 'localhost:9200'
export const ELASTIC_INDEX = process.env.ELASTIC_INDEX || 'finny-app'

export const excludeFiles = ['.DS_Store']

export const GOOGLE_ACCESS_KEY = process.env.GOOGLE_ACCESS_KEY || 'AIzaSyAiismUMyIX5hI46NzQY6oAtiSnsvfSSXA'