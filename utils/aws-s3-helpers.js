import _ from 'lodash'
import path from 'path'
import Promise from 'bluebird'
import AWS from 'aws-sdk'
import { errorObj, logger } from './func-helpers'

export default class Aws3Helper {
  constructor () {
    this.accessKeyId = null
    this.secretAccessKey = null
    this.region = null
    this.bucketKey = null
    this.bucketName = null
    this.s3 = null
  }

  init () {
    try {
      this.s3 = new AWS.S3({
        accessKeyId: this.accessKeyId,
        secretAccessKey: this.secretAccessKey,
        region: this.region
      })
    } catch (e) {
      throw new Error (e)
    }
  }

  listProperty () {
    const bucketName = this.bucketName

    return {
      bucketKey: this.bucketKey,
      mediaUrl: `//${bucketName}.s3.amazonaws.com/`,
      bucket: bucketName
    }
  }

  async createFolder (data) {
    const s3 = this.s3
    let params = _.merge({
      Bucket: this.bucketName,
    }, data || {})

    return new Promise((resolve, reject) => {
      logger.log('Creating s3 folder')
      try {
        s3.headObject(params, (err, data) => {
          if (err) {
            logger.log('s3 folder creation failed')
            reject(errorObj({
              status: false,
              message: err.message
            }))
          } else if (!err) {
            return uploadMedia(params)
          }
        })
      } catch (e) {
        reject(errorObj({
          message: e.stack
        }))
      }
    })
  }

  async listMedia (data) {
    const s3 = this.s3
    let params = _.merge({
      Bucket: this.bucketName
    }, data || {})

    return new Promise((resolve, reject) => {
      try {
        logger.log('Listing s3 assets')
        s3.listObjectsV2(params, (err, data) => {
          if (err) {
            logger.log('s3 list media failed')
            reject(errorObj({
              status: false,
              message: err.message
            }))
          }

          if (data) {
            logger.log('s3 list media successful')
            resolve(data)
          }
        })
      } catch (e) {
        reject(errorObj({
          message: e.stack
        }))
      }
    })
  }

  async uploadMedia (data) {
    const s3 = this.s3
    let params = _.merge({
      Bucket: this.bucketName
    }, data || {})

    return new Promise((resolve, reject) => {
      try {
        logger.log('Uploading new s3 asset')
        s3.putObject(params, (err, data) => {
          if (err) {
            logger.log('s3 upload failed')
            reject(errorObj({
              status: false,
              message: err.message
            }))
          }

          if (data) {
            logger.log('s3 upload successful')
            resolve(data)
          }
        })
      } catch (e) {
        reject(errorObj({
          message: e.stack
        }))
      }
    })
  }

  async deleteFolder (data) {
    const s3 = this.s3
    let params = _.merge({
      Bucket: this.bucketName
    }, data || {})

    return new Promise((resolve, reject) => {
      try {
        logger.log('Deleting s3 folder')
        this.deleteItem(params).then((results) => {
          logger.log('s3 folder deletion successful')
          resolve(results)
        }, (err) => {
          logger.log('s3 folder deletion failed')
          reject(err)
        })
      } catch (e) {
        reject(errorObj({
          message: e.stack
        }))
      }
    })
  }

  async getItem (dataParams) {
    const s3 = this.s3
    let params = _.merge({
      Bucket: this.bucketName
    }, dataParams || {})

    // return new Promise((resolve, reject) => {
      try {
        logger.log('Getting s3 item')
        const {err, data} = await s3.getObject(params)
        console.log(err, data)

        

        return data
      } catch (e) {
        return errorObj({
          message: e.stack
        })
      }
    // })
  }

  async createItem (data) {
    const s3 = this.s3

    let params = _.merge({
      Bucket: this.bucketName
    }, data || {})

    return new Promise((resolve, reject) => {
      try {
        logger.log('Creating s3 item')
        s3.upload(params, (err, data) => {
          if (err) {
            logger.log('s3 item upload failed')
            reject(errorObj({
              status: false,
              message: err.message
            }))
          }
          
          if (data) {
            logger.log('s3 item upload successful')
            resolve({
              status: true,
              message: 'successful!',
              data: data
            })
          }
        })
      } catch (e) {
        reject(errorObj({
          message: e.stack
        }))
      }
    })
  }

  async deleteItem (data) {
    const s3 = this.s3

    let params = _.merge({
      Bucket: this.bucketName
    }, data || {})

    return new Promise((resolve, reject) => {
      try {
        logger.log('Deleting s3 item')
        s3.deleteObject(params, (err, data) => {
          if (err) {
            logger.log('s3 item deletion failed')
            reject(errorObj({
              status: false,
              message: err.message
            }))
          }

          if (data) {
            logger.log('s3 item deletion successful')
            resolve({
              status: true,
              message: 'successful!',
              data: data
            })
          }
        })
      } catch (e) {
        reject(errorObj({
          message: e.stack
        }))
      }
    })
  }
}
