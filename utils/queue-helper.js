import QueueBull from 'bull'

export default class QueueService {
  constructor () {
    this.Queue = null
    // this.redisUrl = process.env.REDIS_URL || 'redis://127.0.0.1:6379'
    this.redisUrl = 'redis://localhost:6379'
    this.limiter = {
      max: 3,
      duration: 10000
    }
    this.settings = {
      backoffStrategies: {
        jitter (attemptsMade, err) {
          return 5000 + Math.random(60) * 500;
        }
      }
    }
  }

  create(task) {
    this.Queue = new QueueBull(task || `task-${new Date().getTime()}`, {
      redis: this.redisUrl,
      //limiter: this.limiter,
      settings: this.settings
    })
    return this.Queue
  }
}