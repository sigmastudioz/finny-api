import _ from 'lodash'
import fs from 'fs'
import path from 'path'
import mongoose from 'mongoose'
import shorthash from 'shorthash'
import bcrypt from 'bcrypt-nodejs'
import jwt from 'jsonwebtoken'
import moment from 'moment'
import mkdirp from 'mkdirp'
import ms from 'milliseconds'
import { site_logger, excludeFiles } from './constants'

const cert = fs.readFileSync(path.join(__dirname, '../configs/certs/private.key'))

const allFilesSync = (dir, fileList = []) => {
  fs.readdirSync(dir).forEach(file => {
    const filePath = path.join(dir, file)
    if ((excludeFiles.indexOf(file) === -1)) {
      fileList.push(
        fs.statSync(filePath).isDirectory()
          ? allFilesSync(filePath)
          : filePath
      )
    }
  })

  return _.flattenDeep(fileList)
}


const jsonParser = obj => {
  return typeof obj === 'object' ? obj : JSON.parse(JSON.stringify(obj))
}

const errLog = obj => {
  const err = jsonParser(obj)
  return new Error(err.message);
}

export const mkDir = (uploadDir) => {
  mkdirp.sync(uploadDir)
}


export const requireAll = allFilesSync

export const jsonClean = jsonParser

export const logger = (() => {
  return {
    log () {
      if(site_logger){
        return console.log(arguments)
      }
    }
  }
})()

export const formatDate = (date = '', dateFormat = 'YYYY-MM-DD H:mm:ssA') => {
  return moment(date).format(dateFormat)
}

export const shortIdGen = (data) => {
  try {
    const getCurrentTime = new Date().getTime().toString()
    if (data) {
      return shorthash.unique(`${data}`)
    } else {
      return shorthash.unique(getCurrentTime)
    }

    return shortid
  } catch (e) {
    return new Error(e)
  }
}

export const generateHash = (password, strength) => {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(strength || 8), null)
}

export const validPassword = ({ CurrentPassword, DbPassword }) => {
  return new Promise((resolve, reject) => {
    try {
      if (DbPassword !== '') {
        bcrypt.compare(CurrentPassword, DbPassword, (err, res) => {
          if (err) {
            reject(err)
          }

          if (res) {
            resolve({
              status: true,
              message: 'Valid password'
            })
          } else {
            resolve({
              status: false,
              message: 'Password doesn\'t match!'
            })
          }
        })
      } else {
        resolve({
          status: false,
          message: 'Password hasn\'t been set!'
        })
      }
    } catch (e) {
      reject(errLog({
        status: false,
        message: e
      }))
    }
  })
}

export const AuthVerify = async (token) => await jwt.verify(token, cert, { algorithms: ['RS256'] })

export const AuthTokenize = async (TokenData) => {
  try {
    if (TokenData) {
      const token = await jwt.sign({ signed: TokenData }, cert, {
        algorithm: 'RS256',
        expiresIn: ms.weeks(12)
      })
  
      return {
        status: true,
        message: 'Logged in.',
        token: token,
        data: TokenData
      }
    } else {
      return errorObj({
        status: false,
        message: 'Token Encode Data Required.'
      })
    }
  } catch (e) {
    return errorObj({
      status: false,
      message: e
    })
  }
}


export const convertToObjectId = (id) => {
  return mongoose.Types.ObjectId(id)
}

export const dasherize = (str = '') => {
  return str.replace(/[A-Z]/g, function(char, index) {
    return (index !== 0 ? '-' : '') + char.toLowerCase();
  })
}

export const camelize = (str = '') => {
  return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, (match, index) => {
    if (+match === 0) return '' // or if (/\s+/.test(match)) for white spaces
    return index == 0 ? match.toLowerCase() : match.toUpperCase()
  })
}

export const trim = (str = '') => {
  return str.replace(/^\s+|\s+$/g,'');
}

export const capitalize = (str = '') => {
  return str.split(/\s+/g).map(w => w[0].toUpperCase() + w.substr(1)).join(' ')
}

export const errorObj = errLog