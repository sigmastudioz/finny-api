import _ from 'lodash'
import redis from 'redis'
import bluebird from 'bluebird'
import { cacheUrl, stdCacheTtl } from './constants'
import { shortIdGen, logger, errorObj } from './func-helpers'

logger.log('Loaded Cache Helpers')
const RedisCache = redis.createClient(cacheUrl)
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

RedisCache.on('connect', () => {
  logger.log('Redis Cache Connected')
}).on('error', () => {
  logger.log('Redis Cache Error')
}).on('end', () => {
  logger.log('Redis Cache Connection Closed')
}).on('set', (key, value) => {
  logger.log('Cache Set', key)
}).on('del', (key, value) => {
  logger.log('Cache Del', key)
}).on('expired', (key, value) => {
  logger.log('Cache Expired', key)
  logger.log('ttlNow', Date.now())
}).on('flush', () => {
  logger.log('Cache Flushed')
})
export default class CacheClient {
  
  constructor () {
    this.cache = RedisCache
    this.cacheKey = null
    this.ttl = stdCacheTtl || 60
    this.cacheData = null
    this.cacheKeyList = []
    this.err = new EvalError()
  }

  async getCache ({ cacheKey }) {
    try {
      const cache = this.cache
      const error = this.err
      cacheKey = cacheKey || this.cacheKey
      if (cacheKey) {
        const key = shortIdGen(cacheKey)
        const data = await cache.getAsync(key)

        if (data) {
          logger.log(key, 'Found!')
          return {
            key: key,
            status: true,
            cache: true,
            message: 'Cache found!',
            data: JSON.parse(data)
          }
        } else {
          logger.log(key, 'Can\'t Find!')
          return {
            key: key,
            status: false,
            cached: false,
            message: 'Cache is not found!'
          }
        }
      } else {
        return {
          status: false,
          cached: false,
          message: 'Cache is required or cannot be null!'
        }
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async setCache ({ expire, cacheKey, cacheData }) {
    const ttl = expire || this.ttl
    cacheKey = cacheKey || this.cacheKey
    cacheData = cacheData || this.cacheData
    const cache = this.cache
    const error = this.err
    try {
      if (cacheKey) {
        const key = shortIdGen(cacheKey)      
        const data = await cache.setAsync(key, JSON.stringify(cacheData), 'EX', ttl)

        if (data === 'OK') {
          logger.log(key, 'Saved!', 'ttlNow', Date.now())
          return {
            key: key,
            status: true,
            cached: false,
            data: cacheData,
            message: 'Information set!'
          }
        } else {
          logger.log(key, 'Can\'t Save!')
          return {
            key: key,
            status: false,
            cached: false,
            message: 'Oops! Looks like our cache is not working!'
          }
        }
      } else {
        return {
          status: false,
          cached: false,
          message: 'Cache is required or cannot be null!'
        }
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async delCache ({ cacheKey }) {
    try {
      const cache = this.cache
      const error = this.err
      cacheKey = cacheKey || this.cacheKey
      if (cacheKey) {
        const key = shortIdGen(cacheKey)
        const data = await cache.del(key) 

        if (data) {
          logger.log(key, 'Deleted!')
          return {
            key: key,
            status: true,
            cached: false,
            message: 'Information is deleted!'
          }
        } else {
          logger.log(key, 'Can\'t Delete!')
          return {
            key: key,
            status: false,
            cached: false,
            message: 'Oops! Looks like our cache is not working!'
          }
        }
      } else {
        return {
          status: false,
          cached: false,
          message: 'Cache is required or cannot be null!'
        }
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async refreshCache ({ cacheKey, cacheData }) {
    try {
      const getCache = await this.getCache({
        cacheKey
      })
      if (getCache.status) {
        await this.delCache({
          cacheKey
        })
      }

      return await this.setCache({
        cacheKey,
        cacheData
      })
    } catch (e) {
      return new Error(e)
    }
  }

}