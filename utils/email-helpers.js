const email = require('emailjs')
import { logger } from './func-helpers'

class EmailHelper {
  constructor() {
    logger.log('Loading Email Helpers')
    this.emailServer = email.server.connect({
      user: process.env.SMTP_USERNAME || "apikey",
      password: process.env.SMTP_PASSWORD || "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",
      host: process.env.SMTP_SERVER || 'smtp.sendgrid.net',
      ssl: true
    })
  }

  async sendMail(message) {
    try {
      return await this.emailServer.send(message)
    } catch (e) {
      logger.log(e)
    }
  }
}

export default EmailHelper