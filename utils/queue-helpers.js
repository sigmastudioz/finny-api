import Queue from 'bee-queue'
// // import Parallel from 'paralleljs'

export default class QueueService {
  constructor () {
    this.prefix = 'nibble'
    this.stallInterval = 5000
    this.nearTermWindow = 1200000
    this.delayedDebounce = 1000
    this.TIMEOUT = 3000
    this.Queue = Queue
    this.redis = {
      host: 'localhost',
      port: 6379,
      db: 0,
      options: {}
    }
    this.queueName = new Date().getTime()
    this.isWorker = true
    this.getEvents = true
    this.sendEvents = true
    this.storeJobs = true
    this.ensureScripts = true
    this.activateDelayedJobs = true
    this.removeOnSuccess = true
    this.removeOnFailure = true
    this.redisScanCount = true
    this.currentQueueProcess = null
  }

  taskManager () {
    try {
      this.currentQueueProcess = new this.Queue(this.queueName, {
        prefix: this.prefix,
        stallInterval: this.stallInterval,
        nearTermWindow: this.nearTermWindow,
        delayedDebounce: this.delayedDebounce,
        redis: this.redis,
        isWorker: this.isWorker,
        getEvents: this.getEvents,
        sendEvents: this.sendEvents,
        storeJobs: this.storeJobs,
        ensureScripts: this.ensureScripts,
        activateDelayedJobs: this.activateDelayedJobs,
        removeOnSuccess: this.removeOnSuccess,
        removeOnFailure: this.removeOnFailure,
        redisScanCount: this.redisScanCount
      });

      return this.currentQueueProcess
    } catch (e) {
      throw new Error(e)
    }
  } 

  async checkHealth() {
    try {
      return await this.currentQueueProcess.checkHealth()
    } catch (e) {
      throw new Error(e)
    }
  }

  startJob (input, callback) {
    return new Promise(async (resolve, reject) => {
      try {
        const Queue = this.currentQueueProcess
        console.log(input)
        const Job = Queue.createJob(input)
        const Process = await Job.timeout(this.TIMEOUT)
          .retries(2)
          .save()

        // console.log(Process)
        
        Process.on('uncaughtException', async () => {
          // Queue#close is idempotent - no need to guard against duplicate calls.
          try {
            await Queue.close(this.TIMEOUT);
          } catch (err) {
            console.error('bee-queue failed to shut down gracefully', err);
            reject()
          }
          Process.exit(1);
        });

        // const results = await Queue.process(async (job) => {
        //   console.log(`Processing job ${job.id}`);
        //   if (typeof callback === 'function') {
        //     return callback.apply(job)
        //   }
        // });

        Queue.process((job) => {
          console.log(`Processing job ${job.id}`);
          resolve(job)
        })
      } catch (e) {
        throw new Error(e)
      }
    })
  }

  // async bulkJobCreate (jobs = [], callback) {
  //   // const bulkCreate = new Parallel(jobs, {
  //     env: {
  //       startJob: this.startJob,
  //       callback: callback,
  //     },
  //     envNamespace: 'parallel'
  //   })

  //   const processJob = (n) => {
  //     //startJob(n, callback)
  //     console.log(global.parallel)
  //     // global.queueHelper.startJob(n, global.queueHelper.callback)
  //     return {}
  //   }

  //   bulkCreate.map(processJob)
  // }
}