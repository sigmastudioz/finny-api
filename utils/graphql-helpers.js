import { GraphQLScalarType } from 'graphql'
import GraphQLJSON from 'graphql-type-json'
import { Kind } from 'graphql/language'
import _ from 'lodash'
import { convertToObjectId, formatDate, errorObj, logger, requireAll, generateHash, shortIdGen } from './func-helpers'

export default class GraphqlHelper {
  constructor () {
    this.RootQuery = `
      scalar Date
      scalar JSON
      scalar ObjectID
      scalar ShortHash
      scalar Password

      input ID {
        _id: ObjectID!
      }

      type File {
        filename: String!
        mimetype: String!
        encoding: String!
      }

      type ResponseMessage {
        status: Boolean
        data: JSON
        message: String
      }
    `

    this.SchemaDefinition = `
      schema {
        query: Query,
        mutation: Mutation
      }
    `
    // /,
    // subscription: Subscription
    this.Mutation = {}
    this.Query = {}
    // this.Subscription = {}
    this.resolvers = {}
    this.typeDefs = []
  }

  GraphQl () {
    try {
      logger.log('Generating GraphQL Schema')
      this.resolvers = _.merge({
        JSON: GraphQLJSON,
        Date: new GraphQLScalarType({
          name: 'Date',
          description: 'Date custom scalar type',
          parseValue(value) {
            return new Date(value); // value from the client
          },
          serialize(value) {
            return value
            // value sent to the client
          },
          parseLiteral(ast) {
            if (ast.kind === Kind.INT) {
              return parseInt(ast.value, 10); // ast value is always in string format
            }
            return null;
          },
        }),
        ObjectID: new GraphQLScalarType({
          name: 'ObjectID',
          description: 'Mongo DB custom Object ID type',
          parseValue(value) {
            return convertToObjectId(value); // value from the client
          },
          serialize(value) {
            if (typeof value === 'string') {
              return value
            } else {
              return value.toString()
            }
            // value sent to the client
          },
          parseLiteral(ast) {
            if (ast.kind === Kind.STRING) {
              return convertToObjectId(ast.value); // ast value is always in string format
            }
            return null;
          },
        }),
        Password: new GraphQLScalarType({
          name: 'Password',
          description: 'Mongo DB custom Password',
          parseValue(value) {
            return generateHash(value); // value from the client
          },
          serialize(value) {
            if (typeof value === 'string') {
              return value
            } else {
              return value.toString()
            }
            // value sent to the client
          },
          parseLiteral(ast) {
            if (ast.kind === Kind.STRING) {
              return generateHash(ast.value); // ast value is always in string format
            }
            return null;
          },
        }),
        ShortHash: new GraphQLScalarType({
          name: 'ShortHash',
          description: 'Mongo DB custom ShortHash',
          parseValue(value) {
            return shortIdGen(value); // value from the client
          },
          serialize(value) {
            if (typeof value === 'string') {
              return value
            } else {
              return value.toString()
            }
            // value sent to the client
          },
          parseLiteral(ast) {
            if (ast.kind === Kind.STRING) {
              return shortIdGen(ast.value); // ast value is always in string format
            }
            return null;
          },
        })
      }, this.resolvers)

      return {
        typeDefs: [this.RootQuery, this.SchemaDefinition, ...this.typeDefs],
        resolvers: this.resolvers
      }
    } catch (e) {
      throw new Error(e)
    }
  }

  async generateTypeDefs ({ dirname }) {
    let queries = []
    let mutations = []
    // let subscriptions = []
    this.typeDefs = []

    try {
      logger.log('Loading GraphQL TypeDefs')
      const files = requireAll(dirname)
      const fileSize = _.size(files)

      if (fileSize > 0) {
        for (let i = 0, len = fileSize; i < len; i++) {
          const file = files[i]
          const fileList = file.split('/')
          const fileName = fileList[_.size(fileList) - 1]
          
          if (file && file.match(/\.[0-9a-z]{1,2}$/g) !== null) {
            const fileInfo = require(file)
          
            if (typeof fileInfo.default === 'string') {
              this.typeDefs.push(fileInfo.default)
            }

            if (typeof fileInfo.Query === 'string') {
              queries.push(fileInfo.Query)
            }

            if (typeof fileInfo.Mutation === 'string') {
              mutations.push(fileInfo.Mutation)
            }

            // if (typeof fileInfo.Subscription === 'string') {
            //   subscriptions.push(fileInfo.Subscription)
            // }
          }
        }

        if (_.size(queries) > 0) {
          this.typeDefs.push(`
              type Query {
                ${queries.join('\n')}
              }
            `
          )
        }

        if (_.size(mutations) > 0) {
          this.typeDefs.push(`
              type Mutation {
                ${mutations.join('\n')}
              }
            `
          )
        }

        // if (_.size(subscriptions) > 0) {
        //   this.typeDefs.push(`
        //       type Subscription {
        //         ${subscriptions.join('\n')}
        //       }
        //     `
        //   )
        // }

        return this.typeDefs
      } else {
        return errorObj({
          status: false,
          message: 'No Files'
        })
      }
    } catch (e) {
      throw new Error(e)
    }
  }

  async generateResolvers ({ dirname }) {
    this.resolvers = {}
    try {
      logger.log('Loading GraphQL Resolvers')
      const files = requireAll(dirname)
      const fileSize = _.size(files)
      
      if (fileSize > 0) {
        for (let i = 0, len = fileSize; i < len; i++) {
          const file = files[i]
          const fileList = file.split('/')
          const fileName = fileList[_.size(fileList) - 1]
          
          if((fileName.match(/\.[0-9a-z]{1,2}$/) || []).indexOf('.js') !== -1) {
            const resolversFile = require(file)
            
            if (typeof resolversFile.Resolvers === 'object') {
              this.resolvers = _.merge(this.resolvers, resolversFile.Resolvers)
            }
  
          }
        }
                
        return this.resolvers
      } else {
        return errorObj({
          status: false,
          message: 'No Files'
        })
      }
    } catch (e) {
      throw new Error(e)
    }
  }
}