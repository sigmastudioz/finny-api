import MapsClient from '@google/maps'

// https://github.com/googlemaps/google-maps-services-js

export default class GoogleMapsHelper {
  constructor ({ key }) {
    this.key = key
    this.googleMapsClient = MapsClient.createClient({
      key: key,
      Promise: Promise
    });
  }
  
  async geocode ({ address }) {
    try {
      return await this.googleMapsClient.geocode({ address }).asPromise()
      .then(({ json } ) => {
        return json
      })
      .catch((err) => {
        console.log(err);
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getPlace (data) {
    try {
      return await this.googleMapsClient.place(data).asPromise()
      .then(({ json } ) => {
        return json
      })
      .catch((err) => {
        console.log(err);
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getPlaces (data) {
    try {
      return await this.googleMapsClient.places(data).asPromise()
      .then(({ json } ) => {
        return json
      })
      .catch((err) => {
        console.log(err);
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getPlacesPhoto (data) {
    try {
      return await this.googleMapsClient.placesPhoto(data).asPromise()
      .then((response) => {
        return response.client.parser.socket.ssl.owner.ssl
        //['Symbol(connect-options)']ssl._httpMessage.ClientRequest().href
      })
      .catch((err) => {
        console.log(err);
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getPlacesAutoComplete (data) {
    try {
      return await this.googleMapsClient.placesAutoComplete(data).asPromise()
      .then(({ json } ) => {
        return json
      })
      .catch((err) => {
        console.log(err);
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getPlacesQueryAutoComplete (data) {
    try {
      return await this.googleMapsClient.placesQueryAutoComplete(data).asPromise()
      .then(({ json } ) => {
        return json
      })
      .catch((err) => {
        console.log(err);
      })
    } catch (e) {
      return new Error(e)
    }
  }
  
  async placesNearby (data) {
    try {
      return await this.googleMapsClient.placesNearby(data).asPromise()
      .then(({ json } ) => {
        return json
      })
      .catch((err) => {
        console.log(err);
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getDirections (data) {
    try {
      return await this.googleMapsClient.directions(data).asPromise()
      .then(({ json } ) => {
        return json
      })
      .catch((err) => {
        console.log(err);
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getDistanceMatrix (data) {
    try {
      return await this.googleMapsClient.distanceMatrix(data).asPromise()
      .then(({ json } ) => {
        return json
      })
      .catch((err) => {
        console.log(err);
      })
    } catch (e) {
      return new Error(e)
    }
  }
  
}