import elasticsearch from 'elasticsearch'
import bodybuilder from 'bodybuilder'
import _ from 'lodash'
import { errorObj, logger } from '../utils/func-helpers'

export default class ElasticSearchHelper {
  constructor () {
    logger.log('Loaded Elastic Search Helpers');
    this.elasticClient = null
    this.indexName = null
    this.indexType = "document" 
    this.indexData = null
    this.indexDataId = null
    this.indexDataProperties = null
    this.queryData = {}
    this.searchUrl = null
    this.search = bodybuilder()
  }

  connect () {
    try {
      this.elasticClient = new elasticsearch.Client({  
        host: this.searchUrl,
        log: 'info',
        // plugins: [ deleteByQuery ]
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async initIndex() {  
    try {
      return await this.elasticClient.indices.create({
        index: this.indexName,
        body: this.indexData
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async indexExists() {  
    try {
      return await this.elasticClient.indices.exists({
        index: this.indexName
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteIndex() {  
    try {
      return await this.elasticClient.indices.delete({
        index: this.indexName
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async initMapping() {  
    try {
      return await this.elasticClient.indices.putMapping({
        index: this.indexName,
        type: this.indexType,
        body: this.indexDataProperties
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async addDocument() {  
    try {
      console.log({
        index: this.indexName,
        id: this.indexDataId,
        type: this.indexType,
        body: this.indexData
      })
      return await this.elasticClient.index({
        index: this.indexName,
        id: this.indexDataId,
        type: this.indexType,
        body: this.indexData
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async bulkInjestDocuments(){
    try {
      return await this.elasticClient.bulk({
        body: this.indexData
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updateDocument() {
    try {  
      return await this.elasticClient.update({
        index: this.indexName,
        id: this.indexDataId,
        type: this.indexType,
        body: this.indexData
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteDocument() {  
    try {
      return await this.elasticClient.delete({
        index: this.indexName,
        id: this.indexDataId,
        type: this.indexType
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async searchIndex() {  
    try {
      const searchData = this.queryData
      return await this.elasticClient.search(_.merge({
        index: this.indexName,
      }, searchData))
    } catch (e) {
      return new Error(e)
    }
  }

  async getSuggestions(){
    try {
      return await this.elasticClient.suggest({
        index: this.indexName,
        type: this.indexType,
        body: this.indexData
      })
    } catch (e) {
      return new Error(e)
    }
  }
}