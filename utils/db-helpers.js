import _ from 'lodash'
import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { errorObj, logger, requireAll, capitalize, camelize, dasherize } from './func-helpers'

mongoose.Promise = global.Promise
const Schema = mongoose.Schema

export default class DBHelpers {
  constructor() {
    this.Model = null
    this.ModelData = null
    this.sortBy = null
    this.ModelPopulate = []
    this.ModelIncludes = {}

    this.searchQuery = {}
    this.page = 1
    this.limit = 50
    this.sortBy = 'createdAt'
  }

  async startDb({ db, port }) {
    try {
      let database = await mongoose.connect(db, {
        useNewUrlParser: true
      });
      mongoose.connection.on('error', (err) => {
        logger.log(`Error: Could not connect to MongoDB. Did you forget to run mongod?`);
      }).on('open', () => {
        logger.log(`Connection established with MongoDB`)
      });
      return database;
    } catch (e) {
      return new Error(e)
    }
  }

  async createModel ({ Model = null, ModelData = {} }) {
    try {
      ModelData.createdAt = new Date()
      const DbModel = new Model(ModelData)

      const data = await DbModel.save()
      
      if (data) {
        return {
          status: true,
          data
        }
      } else {
        return errorObj({
          status: false,
          message: 'database find error!'
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async findModel ({ Model = null, query = {}, includes = {}}) {
    try {
      const data = await Model.findOne(query, includes)
        .exec()

      if (data) {
        return {
          status: true,
          data
        }
      } else {
        return {
          status: false,
          message: 'database find error!'
        }
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async findAllModel ({ Model = null, query = {}}) {
    try {
      const searchQuery = query
      const dataQuery = searchQuery.query || {}
      const page = searchQuery.page || this.page
      const limit = searchQuery.limit || this.limit
      const sortBy = searchQuery.sortBy || this.sortBy

      if (_.has(searchQuery, 'page')) {
        delete searchQuery.page
      }

      if (_.has(searchQuery, 'limit')) {
        delete searchQuery.limit
      }

      if (_.has(searchQuery, 'query')) {
        delete searchQuery.query
      }

      const data =  await Model.find(dataQuery)
      // const data =  await Model.aggregate(query)
        // .populate(this.ModelPopulate)
        .skip(limit * ((page || 1) - 1))
        .limit(limit)
        .sort(sortBy)

      const count = await Model.countDocuments(dataQuery)

      if (data && count) {
        return {
          status: true,
          docs: data,
          total: count,
          limit: limit,
          page: page,
          pages: Math.ceil(count / limit) || 1
        }
      } else {
        return {
          status: false,
          docs: [],
          total: 0,
          limit: limit,
          page: 1,
          pages: 1
        }
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async findAllModelAggregate ({ Model = null, query = {}}) {
    try {
      const searchQuery = query
      const dataQuery = searchQuery.query || {}
      const page = searchQuery.page || this.page
      const limit = searchQuery.limit || this.limit
      const sortBy = searchQuery.sortBy || this.sortBy

      if (_.has(searchQuery, 'page')) {
        delete searchQuery.page
      }

      if (_.has(searchQuery, 'limit')) {
        delete searchQuery.limit
      }

      if (_.has(searchQuery, 'query')) {
        delete searchQuery.query
      }

      const data =  await Model.aggregate(dataQuery)
        // .skip(limit * ((page || 1) - 1))
        // .limit(limit)
        // .sort(sortBy)
      console.log('findAllModelAggregate ->', dataQuery, data)
      

      const count = await Model.countDocuments(dataQuery)

      if (data && count) {
        return {
          status: true,
          docs: data,
          total: count,
          limit: limit,
          page: page,
          pages: Math.ceil(count / limit) || 1
        }
      } else {
        return {
          status: false,
          docs: [],
          total: 0,
          limit: limit,
          page: 1,
          pages: 1
        }
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async updateModel ({ Model = null, ModelData = {}, searchQuery = {} }) {
    try {
      if (_.has(ModelData, 'id')) {
        delete ModelData._id
      }

      if (_.has(ModelData, 'createdAt')) {
        delete ModelData.createdAt
      } 

      ModelData.updatedAt = new Date()
      const data = await Model.findOneAndUpdate(searchQuery.query, { $set: ModelData }, { upsert: true, new: true })

      if (data) {
        return {
          data,
          status: true
        }
      } else {
        return errorObj({
          status: false,
          message: 'database update error!'
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async removeModel ({ Model = null, searchQuery = {} }) {
    try {
      const data = await Model.findOneAndRemove(searchQuery.query)
      
      if (data) {
        return {
          data,
          status: true
        }
      } else {
        return errorObj({
          status: false,
          message: 'database remove error!'
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async updateSingle ({ Model = null, ModelData = {}, searchQuery = {} }){
    try {
      const updateModel = await this.updateModel({ Model, ModelData, searchQuery })
      return updateModel
    } catch (e) {
      return new Error(e)
    }
  }


  async deleteSingle ({ Model = null, searchQuery = {} }){
    try {
      const removeModel = await this.removeModel({ Model, searchQuery })
      return removeModel
    } catch (e) {
      return new Error(e)
    }
  }

  async bulkUpdate ({ Model = null, searchQuery = {}, newData }) {
    try {
      newData.updatedAt = new Date()
      const data = await Model.updateMany(searchQuery.query, {$set: newData}, { upsert: true, multi: true })

      if (data) {
        return {
          data,
          status: true
        }
      } else {
        return errorObj({
          status: false,
          message: 'database remove error!'
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async bulkDelete ({ Model = null, searchQuery = {} }) {
    try {
      const data =  await Model.deleteMany(searchQuery.query)
      if (data) {
        return {
          data,
          status: true
        }
      } else {
        return errorObj({
          status: false,
          message: 'database remove error!'
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async mongooseModels ({ dirname }) {
    let model = {}
    const err = new EvalError()
  
    try {
      logger.log('Loading Mongoose Models')
      const files = requireAll(dirname)
      const fileSize = _.size(files)
      
      if (fileSize > 0) {
        for (let i = 0, len = fileSize; i < len; i++) {
          const file = files[i]
          const fileList = file.split('/')
          const fileName = fileList[_.size(fileList) - 1]
  
          if (file && file.match(/\.[0-9a-z]{1,2}$/g) !== null) { 
            const modelInfo = require(file) 
            const modelFile = modelInfo.default   
            const modelPreHook = modelInfo.PreHook || null
            const modelPostHook = modelInfo.PostHook || null
  
            if (typeof modelFile === 'object') {
              const modelSchema = new Schema(modelFile)
              const fileInfo = capitalize(camelize(fileName.replace('.js', ''))).replace(/[^\w\s]/gi, '');
             
              if (modelPreHook && typeof modelPreHook === 'object') {
                _.each(modelPreHook, (func, index) => {
                  modelSchema.pre(index, func)
                })
              }
  
              if (modelPostHook && typeof modelPostHook === 'object') {
                _.each(modelPostHook, (func, index) => {
                  modelSchema.post(index, func)
                })
              }
  
              modelSchema.plugin(autopopulate)
              let Model = mongoose.model(dasherize(fileInfo), modelSchema)
  
              if(typeof model[`${fileInfo}`] === 'undefined'){
                model[`${fileInfo}`] = Model
              } 
            }
          }
        }
  
        return model
      } else {
        return errorObj({
          status: false,
          message: 'No files'
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }
}