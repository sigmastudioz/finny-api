import bluebird from 'bluebird'
global.Promise = bluebird

import express from 'express'
import bodyParser from 'body-parser'
import compression from 'compression'
import logger from 'morgan'
import path  from 'path'
import _ from 'lodash'

import { ApolloServer, gql, AuthenticationError } from 'apollo-server-express'
import { apolloUploadExpress } from 'apollo-upload-server'
import cors from 'cors'


import AuthHelpers from '../utils/auth-helpers'
import DBCacheHelpers from '../utils/db-cache-helpers'
import Search from '../utils/elastic-search-helpers'
import GraphqlHelpers from '../utils/graphql-helpers'
import CacheClient from '../utils/cache-helpers'
import DBHelpers from '../utils/db-helpers'
import Aws3Helpers from '../utils/aws-s3-helpers'
import GoogleMapsHelpers from '../utils/google-maps-helpers'
import * as Helpers from '../utils/func-helpers'
import EmailHelpers from '../utils/email-helpers'

import { DATABASE_URL, ELASTIC_URL, ELASTIC_INDEX, AWS_BUCKET_NAME, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_REGION, AWS_BUCKET_KEY, GOOGLE_ACCESS_KEY } from '../utils/constants'


const app = express()

// parsing the request body
app.use(compression())
app.use(cors())
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json({limit: '50mb'}));
app.use(logger('dev'))
app.set('port', process.env.PORT || 3030)

// Initialize S3 Bucket
const s3 = new Aws3Helpers()
s3.accessKeyId = `${AWS_ACCESS_KEY_ID}`
s3.secretAccessKey = `${AWS_SECRET_ACCESS_KEY}`
s3.region = `${AWS_REGION}`
s3.bucketName = `${AWS_BUCKET_NAME}`
s3.bucketKey = `${AWS_BUCKET_KEY}`
s3.init()

const startApplication = async () => {
  try {
    const EmailSend = new EmailHelpers()
    const Graph = new GraphqlHelpers()
    await Graph.generateTypeDefs({
      dirname: path.resolve(__dirname, '../api/typedefs')
    })
    
    await Graph.generateResolvers({
      dirname: path.resolve(__dirname, '../api/resolvers')
    })
    
    const GraphQl = Graph.GraphQl()
    
    const ElasticSearch = new Search()
    ElasticSearch.searchUrl = ELASTIC_URL
    ElasticSearch.indexName = ELASTIC_INDEX
    ElasticSearch.connect()

    const GoogleMaps = new GoogleMapsHelpers({
      key: GOOGLE_ACCESS_KEY
    })

    const DbInfo = {}
    DbInfo.cache = new CacheClient()
    DbInfo.db = new DBHelpers()
    await DbInfo.db.startDb({
      db: DATABASE_URL
    })
    const Models = await DbInfo.db.mongooseModels({
      dirname: path.resolve(__dirname, '../api/models')
    })
    
    const Auth = new AuthHelpers()
    Auth.nonAuthPaths = [
      'REGISTER_USER',
      'LOGIN_USER'
    ]
    const server = new ApolloServer(_.merge(GraphQl, {
      graphql: false,
      // playground: {
      //   endpoint: '/graphql'
      // },
      onHealthCheck: () =>
      new Promise((resolve, reject) => {
        //database check or other asynchronous action
      }),
      context: async ({ req }) => {
        const Authorization = await Auth.JwtVerify(req)

        return {
          Authorization,
          DBCacheHelpers,
          DbInfo,
          Models,
          ElasticSearch,
          GoogleMaps,
          CacheClient,
          Helpers,
          EmailSend,
          s3
        }
      }
    }))

    server.applyMiddleware({ app, path: '/api' });

    /**
     * Start Express server.
     */
    app.listen({ port: app.get('port') }, () =>
      console.log(`🚀 Server ready at http://localhost:${app.get('port')}${server.graphqlPath}`)
    )

  } catch (e) {
    throw new Error(e)
  }
}


startApplication()
module.exports = app