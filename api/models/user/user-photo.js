import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  url:{
    type: String,
    required: [true, 'Url is required!'],
    default: null
  },
  key:{
    type: String,
    default: null
  },
  user: { 
    type: Schema.Types.ObjectId, 
    ref: 'user',
    default: null,
    required: [true, 'User ID is required!']
  },
  type:{
    type: String,
    required: [true, 'Type is required!'],
    default: null
  },
  createdAt: { 
    type: Date, 
    default: Date.now 
  },
  updatedAt: { 
    type: Date, 
    default: Date.now 
  }
}