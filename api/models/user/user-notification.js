import mongoose from 'mongoose'
const Schema = mongoose.Schema

const notificationType = (v) => {
  return ['friend', 'comment', 'like', ].indexOf(v) !== -1
}

const notificationMode = (v) => {
  return ['update', 'request'].indexOf(v) !== -1
}


export default {
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    default: null
  },
  friend: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'Friend is required!'],
    default: null
  },
  message: {
    type: String,
    default: null
  },
  mode: {
    type: String,
    required: [true, 'Notification Mode is required!'],
    validate: [notificationMode, '{VALUE} is not a valid notification mode!']
  },
  type: {
    type: String,
    required: [true, 'Notification Type is required!'],
    validate: [notificationType, '{VALUE} is not a valid notification type!']
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}
