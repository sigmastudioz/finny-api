import mongoose from 'mongoose'
const Schema = mongoose.Schema

const privacyType = (v) => {
  return ['public', 'social', 'private', ].indexOf(v) !== -1
}

export default {
  user: { 
    type: Schema.Types.ObjectId, 
    ref: 'user',
    default: null,
    required: [true, 'User ID is required!']
  },
  verified: {
    type: Boolean,
    default: false
  },
  newProfile: {
    type: Boolean,
    default: true
  },
  delete: {
    type: Boolean,
    default: false
  },
  deleteAt: {
    type: Date,
    default: null
  },
  privacy: {
    type: Boolean,
    default: false
  },
  privacyType: {
    type: String,
    default: 'public',
    validate: [privacyType, '{VALUE} is not a valid privacy type!']
  },
  hidden: {
    type: Boolean,
    default: false
  },
  notificationTypes: {
    type: Schema.Types.Mixed,
    default: {
      comments: true,
      likes: true,
      friends: true
    }
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}