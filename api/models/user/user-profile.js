import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    unique: true,
    default: null
  },
  firstName: {
    type: String,
    required: [true, 'First Name is required!'],
    default: null
  },
  middleName: {
    type: String,
    default: null
  },
  lastName: {
    type: String,
    required: [true, 'Last Name is required!'],
    default: null
  },
  profileSummary: {
    type: String,
    default: null
  },
  dob: {
    type: Date,
    default: null
  },
  gender: {
    type: Schema.Types.ObjectId,
    ref: 'user-gender',
    default: null
  },
  avatar: {
    type: Schema.Types.ObjectId,
    ref: 'user-photo',
    default: null
  },
  image: {
    type: Schema.Types.ObjectId,
    ref: 'user-photo',
    default: null
  },
  referer: {
    type: String,
    default: null
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}