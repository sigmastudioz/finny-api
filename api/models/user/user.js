import mongoose from 'mongoose'
import { shortIdGen, logger } from '../../../utils/func-helpers'

const Schema = mongoose.Schema

const checkForUserType = (v) => {
  return (v === 'user' || v === 'business')
}

export default {
  userHash: {
    type: String,
    default: null
  },
  social: {
    type: Schema.Types.ObjectId,
    ref: 'user-social',
    default: null
  },
  profile: {
    type: Schema.Types.ObjectId,
    ref: 'user-profile',
    default: null
  },
  setting: {
    type: Schema.Types.ObjectId,
    ref: 'user-setting',
    default: null
  },
  type: {
    type: String,
    validate: [checkForUserType, '{VALUE} is not a valid user type!'],
    default: 'user'
  },
  // business: {
  //   type: Schema.Types.ObjectId,
  //   ref: 'user-business',
  //   default: null
  // },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}