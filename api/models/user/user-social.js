import mongoose from 'mongoose'
import validator from 'validator'
import { generateHash } from '../../../utils/func-helpers'

const Schema = mongoose.Schema

export default  {
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    unique: true,
    default: null
  },
  local: {
    email: {
      type: String,
      default: null,
      trim: true,
      lowercase: true,
      unique: true,
      validate: [validator.isEmail, 'Please fill a valid email address']
    },
    changeEmail: {
      type: String,
      default: null,
      trim: true,
      lowercase: true
    },
    username: {
      type: String,
      default: null,
      trim: true,
      lowercase: true
    },
    password: {
      type: String,
      default: null
    },
    resetPasswordToken: {
      type: String,
      default: null
    },
    resetPasswordExpires: {
      type: Date,
      default: Date.now
    },
    resetEmailToken: {
      type: String,
      default: null
    }
  },
  facebook: {
    id: {
      type: String,
      default: null
    },
    token: {
      type: String,
      default: null
    },
    name: {
      type: String,
      default: null
    },
    profile: {
      type: String,
      default: null
    }
  },
  twitter: {
    id: {
      type: String,
      default: null
    },
    token: {
      type: String,
      default: null
    },
    displayName: {
      type: String,
      default: null
    },
    username: {
      type: String,
      default: null
    },
    profile: {
      type: String,
      default: null
    }
  },
  google: {
    id: {
      type: String,
      default: null
    },
    token: {
      type: String,
      default: null
    },
    name: {
      type: String,
      default: null
    },
    profile: {
      type: String,
      default: null
    }
  },
  youtube: {
    id: {
      type: String,
      default: null
    },
    token: {
      type: String,
      default: null
    },
    name: {
      type: String,
      default: null
    },
    profile: {
      type: String,
      default: null
    }
  },
  instagram: {
    id: {
      type: String,
      default: null
    },
    token: {
      type: String,
      default: null
    },
    name: {
      type: String,
      default: null
    },
    profile: {
      type: String,
      default: null
    }
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}