import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    default: null
  },
  friend: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    default: null
  },
  approved: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}
