import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    default: null
  },
  place_id: {
    type: String,
    default: null,
    required: [true, 'Place is required!'],
  },
  title: {
    type: String,
    min: [5, '5 is the minimum!'],
    max: [80, '80 is the maximum!']
  },
  comment: {
    type: String,
    min: [5, '5 is the minimum!'],
    max: [300, '300 is the maximum!']
  },
  place_photo: [{ 
    type: Schema.Types.ObjectId, 
    ref: 'place-photo',
    default: null
  }],
  checked_in_with_friends: [{
    type: Schema.Types.ObjectId,
    ref: 'user',
    default: null
  }],
  geometry: {
    type: Schema.Types.Mixed,
    default: null,
    required: [true, 'Geometry is required!'],
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}
