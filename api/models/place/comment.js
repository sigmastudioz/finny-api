import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  user: { 
    type: Schema.Types.ObjectId, 
    ref: 'user',
    default: null,
    required: [true, 'User is required!']
  },
  comment:{
    type: String,
    required: [true, 'Comment is required!'],
    default: null
  },
  place_id: {
    type: String,
    default: null,
    required: [true, 'Place is required!'],
  },
  rate: {
    type: Number,
    required: [true, 'Rating is required!'],
    min: [0, '0 is the minimium!'],
    max: [5, '5 is the maximium!'],
    default: null
  },
  flagCount: {
    type: Number,
    default: 0
  },
  blocked: {
    type: Boolean,
    default: false
  },
  hidden: {
    type: Boolean,
    default: false
  },
  createdAt: { 
    type: Date, 
    default: Date.now 
  },
  updatedAt: { 
    type: Date, 
    default: Date.now 
  }
}
