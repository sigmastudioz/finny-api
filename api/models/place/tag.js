import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  name: {
    type: 'String',
    trim: true,
    required: [true, 'Name is required!'],
    unique: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    default: null
  },
  place: {
    type: Schema.Types.ObjectId,
    ref: 'place',
    required: [true, 'Place is required!'],
    default: null
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}