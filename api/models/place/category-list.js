import mongoose from 'mongoose'
const Schema = mongoose.Schema

const privacyType = (v) => {
  return ['public', 'private'].indexOf(v) !== -1
}

export default {
  category: {
    type: Schema.Types.ObjectId,
    ref: 'category',
    required: [true, 'Category is required!'],
    default: null
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    default: null
  },
  place: {
    type: Schema.Types.ObjectId,
    ref: 'place',
    required: [true, 'Place is required!'],
    default: null
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}

