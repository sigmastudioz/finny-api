import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    default: null
  },
  place: {
    type: Schema.Types.ObjectId,
    ref: 'place',
    required: [true, 'Place is required!'],
    default: null
  },
  tag: [{
    type: Schema.Types.ObjectId,
    ref: 'tag',
    required: [true, 'Tag is required!'],
    default: null
  }],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}
