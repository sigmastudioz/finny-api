import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    default: null
  },
  place_id: {
    type: String,
    default: null,
    required: [true, 'Place is required!'],
  },
  name: {
    type: String,
    default: null,
    required: [true, 'Name is required!'],
  },
  formatted_address: {
    type: String,
    default: null,
    required: [true, 'Formatted Address is required!'],
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}

