import mongoose from 'mongoose'
const Schema = mongoose.Schema

const privacyType = (v) => {
  return ['public', 'private'].indexOf(v) !== -1
}

export default {
  name: {
    type: 'String',
    trim: true,
    required: [true, 'Name is required!']
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'User is required!'],
    default: null,
    autopopulate: false
  },
  privacyType: {
    type: String,
    default: 'public',
    validate: [privacyType, '{VALUE} is not a valid privacy type!']
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}

