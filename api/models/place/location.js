import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  name: {
    type: 'String',
    trim: true,
    required: [true, 'Name is required!']
  },
  locationID: {
    type: 'String',
    trim: true,
    required: [true, 'Google Location ID is required!'],
    unique: true
  },
  address1: {
    type: String,
    required: [true, 'Address is required!'],
    default: null
  },
  address2: {
    type: String,
    default: null
  },
  address3: {
    type: String,
    default: null
  },
  city: {
    type: String,
    required: [true, 'City is required!'],
    default: null
  },
  state: {
    type: String,
    required: [true, 'State is required!'],
    default: null
  },
  zipCode: {
    type: String,
    required: [true, 'Zip Code is required!'],
    min: [5, '5 is the minimium!'],
    default: null
  },
  country: {
    type: String,
    required: [true, 'Country is required!'],
    default: 'United States'
  },
  telephone: {
    type: String,
    default: null
  },
  email: {
    type: String,
    default: null,
    trim: true,
    lowercase: true,
    // validate: [validator.isEmail, 'Please fill a valid email address']
  },
  fax: {
    type: String,
    default: null
  },
  geoPoint: {
    lon: {
      type: Number,
      default: 0
    },
    lat: {
      type: Number,
      default: 0
    }
  },
  hours: [{
    startTime: {
      type: String,
      default: '00:00'
    },
    endTime: {
      type: String,
      default: '00:00'
    },
    timeEnabled: {
      type: Number,
      default: 0
    }
  }],
  type: {
    type: String,
    default: 'Restaurant',
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
}
