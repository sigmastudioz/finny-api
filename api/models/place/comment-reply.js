import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  user: { 
    type: Schema.Types.ObjectId, 
    ref: 'user',
    default: null,
    required: [true, 'User is required!']
  },
  friend: { 
    type: Schema.Types.ObjectId, 
    ref: 'user',
    default: null,
    required: [true, 'Friend is required!']
  },
  comment:{
    type: Schema.Types.ObjectId, 
    ref: 'comment',
    default: null,
    required: [true, 'Comment is required!']
  },
  reply:{
    type: String,
    required: [true, 'Reply is required!'],
    default: null
  },
  place_id: {
    type: String,
    default: null,
    required: [true, 'Place is required!'],
  },
  createdAt: { 
    type: Date, 
    default: Date.now 
  },
  updatedAt: { 
    type: Date, 
    default: Date.now 
  }
}
