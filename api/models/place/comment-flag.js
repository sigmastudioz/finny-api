import mongoose from 'mongoose'
const Schema = mongoose.Schema

export default {
  user: { 
    type: Schema.Types.ObjectId, 
    ref: 'user',
    default: null,
    required: [true, 'User is required!']
  },
  comment:{
    type: Schema.Types.ObjectId, 
    ref: 'comment',
    default: null
  },
  reply:{
    type: Schema.Types.ObjectId, 
    ref: 'reply',
    default: null
  },
  place_id: {
    type: String,
    default: null,
    required: [true, 'Place is required!'],
  },
  createdAt: { 
    type: Date, 
    default: Date.now 
  },
  updatedAt: { 
    type: Date, 
    default: Date.now 
  }
}
