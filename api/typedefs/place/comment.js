const constants = {
  Query: `
    user: ObjectID!
    comment: String!
    place_id: String!
    rate: Int
  `
}

export default `
  type Comment {
    _id: ObjectID
    ${constants.Query}
    flagCount: Int
    blocked: Boolean
    hidden: Boolean
    createdAt: Date
    updatedAt: Date
    Replies: CommentReplyPagination
    Flags: CommentFlagPagination
  }

  type CommentPagination {
    key: String    
    docs: [Comment]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input CommentInput {
    ${constants.Query}
  }
`

export const Query = `
  Comment (query: JSON): Comment
  Comments (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): CommentPagination
`

export const Mutation = `
  createComment (
    input: CommentInput!
  ): Comment

  updateComment (
    _id: ObjectID!
    query: JSON
  ): Comment

  deleteComment (
    _id: ObjectID!
  ): Comment
`