const constants = {
  Query: `
    name: String,
    createdAt: Date,
    updatedAt: Date
  `
}

export default `
  type Tag {
    _id: ObjectID
    ${constants.Query}
  }

  type TagPagination {   
    key: String 
    docs: [Tag]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input TagInput {
    ${constants.Query}
  }
`

export const Query = `
  Tag (query: JSON): Tag
  Tags (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): TagPagination
`

export const Mutation = `
  createTag (
    input: TagInput!
  ): Tag

  updateTag (
    _id: ObjectID!
    query: JSON
  ): Tag

  deleteTag (
    _id: ObjectID!
  ): Tag
`