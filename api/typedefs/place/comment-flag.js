const constants = {
  Query: `
    user: ObjectID!
    place_id: String!
  `
}

export default `
  type CommentFlag {
    _id: ObjectID
    ${constants.Query}
    createdAt: Date
    updatedAt: Date
  }

  type CommentFlagPagination {
    key: String    
    docs: [CommentFlag]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input CommentFlagInput {
    ${constants.Query}
  }
`

export const Query = `
  CommentFlag (query: JSON): CommentFlag
  CommentFlags (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): CommentFlagPagination
`

export const Mutation = `
  createCommentFlag (
    input: CommentFlagInput!
  ): CommentFlag
`