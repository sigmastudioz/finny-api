const constants = {
  Query: `
    name: String
    privacyType: String
  `,
  Mutation: `
    user: ObjectID
  `
}

export default `
  type Category {
    _id: ObjectID
    ${constants.Query}
    user: JSON
    Places(
      page: Int = 1
      limit: Int = 50
      query: JSON
      sortBy: String = "createdAt"
    ): PlacePagination
    createdAt: Date
    updatedAt: Date
  }

  input CategoryQueryInput {
    _id: ObjectID
    ${constants.Query}
    createdAt: Date
    updatedAt: Date
  }

  type CategoryPagination {  
    key: String  
    docs: [Category]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input CategoryInput {
    ${constants.Query}
    ${constants.Mutation}
    Lists: [SearchPlaceInput]
  }
`

export const Query = `
  Category (query: JSON): Category
  Categories (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): CategoryPagination
`

export const Mutation = `
  createCategory (
    input: CategoryInput!
  ): Category

  updateCategory (
    _id: ObjectID!
    query: JSON
  ): Category

  deleteCategory (
    _id: ObjectID!
  ): Category
`