const constants = {
  Query: `
    name: String,
    locationID: String,
    address1: String,
    address2: String,
    address3: String,
    city: String,
    state: String,
    zipCode: String,
    country: String,
    telephone: String,
    email: String,
    fax: String,
    geoPoint: JSON,
    createdAt: Date,
    updatedAt: Date,
    hours: JSON,
    type: String
  `
}

export default `
  type Location {
    _id: ObjectID
    ${constants.Query}
  }

  input LocationGeoPoint {
    lon: Int,
    lat: Int
  }

  input LocationHours {
    startTime: String,
    endTime: String,
    timeEnabled: Int
  }

  type LocationPagination {    
    key: String
    docs: [Location]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input LocationInput {
    ${constants.Query}
  }
`

export const Query = `
  Location (query: JSON): Location
  Locations (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): LocationPagination
`

export const Mutation = `
  createLocation (
    input: LocationInput!
  ): Location

  updateLocation (
    _id: ObjectID!
    query: JSON
  ): Location

  deleteLocation (
    _id: ObjectID!
  ): Location
`