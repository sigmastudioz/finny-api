const constants = {
  Query: `
    url: String!
    key: String
    place_id: String!
    user: ObjectID!
    type: String
  `
}

export default `
  type PlacePhoto {
    _id: ObjectID
    ${constants.Query}
    createdAt: Date
    updatedAt: Date
  }

  type PlacePhotoPagination {   
    key: String 
    docs: [PlacePhoto]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input PlacePhotoInput {
    ${constants.Query}
    file: Upload!
  }
`

export const Query = `
  PlacePhoto (query: JSON): PlacePhoto
  PlacePhotos (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): PlacePhotoPagination
`

export const Mutation = `
  createPlacePhoto (
    input: PlacePhotoInput!
  ): PlacePhoto

  updatePlacePhoto (
    _id: ObjectID!
    query: JSON
  ): PlacePhoto

  deletePlacePhoto (
    _id: ObjectID!
  ): PlacePhoto
`