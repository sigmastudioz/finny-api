const constants = {
  Query: `
    name: String
    user: ObjectID
    place_id: String
    formatted_address: String
  `
}

export default `
  type Place {
    _id: ObjectID
    ${constants.Query}
    addedToCategory(
      category: ObjectID!
    ): JSON
    createdAt: Date
    updatedAt: Date
    AddedToPlace (
      query: JSON!
    ): Boolean
    CheckedIn(
      query: JSON!
    ): Boolean
    CheckIns(
      query: JSON!
    ):CheckInPagination
    Tags(
      query: JSON!
    ):TagPagination
  }

  type PlacePagination {  
    key: String  
    docs: [Place]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input PlaceInput {
    name: String!
    user: ObjectID!
    place_id: String!
    formatted_address: String!
  }
`

export const Query = `
  Place (query: JSON): Place
  Places (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): PlacePagination
`

export const Mutation = `
  createPlace (
    input: SearchPlaceInput
  ): SearchPlace

  updatePlace (
    _id: ObjectID!
    query: JSON
  ): SearchPlace

  deletePlace (
    place_id: String!
    user: ObjectID!
  ): SearchPlace
`