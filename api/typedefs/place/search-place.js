const constants = {
  Query: `
    name: String
    place_id: String!
    formatted_address: String!
    opening_hours: JSON
    geometry: JSON
    price_level: Int
    rating: Float
    types: JSON
    photos: JSON
    formatted_phone_number: String
    international_phone_number: String
    website: String
    vicinity: String
    adr_address: JSON
    review: JSON
  `
}

export default `
  type SearchPlace {
    _id: String
    ${constants.Query}
    AddedToPlace (
      query: JSON!
    ): Boolean
    CheckedIn(
      query: JSON!
    ): Boolean
    Comments: CommentPagination
    Likes: LikePagination
    UsersPhotos: PlacePhotoPagination
    UserCheckIns(
      query: JSON!
    ): CheckInPagination
    Tags: TagPagination
    PlacePhotos: JSON
  }

  type GooglePhoto {
    height: Int
    photo_reference: String
    width: Int
  }

  input SearchPlaceInput {
    photos: JSON!
    user: ObjectID!
    category: ObjectID
  }

  input GetPlaceInput {
    placeid: String!
  }

  type SearchPlacePagination {
    key: String
    nextpagetoken: String  
    docs: [SearchPlace]
  }
`

export const Query = `
  SearchPlaces(query: JSON!): SearchPlacePagination
  GetPlace(query: GetPlaceInput!): SearchPlace
`