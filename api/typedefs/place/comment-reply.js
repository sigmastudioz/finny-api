const constants = {
  Query: `
    user: ObjectID!
    friend: ObjectID!
    place_id: String!
  `
}

export default `
  type CommentReply {
    _id: ObjectID
    ${constants.Query}
    reply: String
    Flags: CommentFlagPagination
    createdAt: Date
    updatedAt: Date
  }

  type CommentReplyPagination {
    key: String    
    docs: [CommentReply]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input CommentReplyInput {
    ${constants.Query}
    comment: ObjectID!
    reply: String!
  }
`

export const Query = `
  CommentReply (query: JSON): CommentReply
  CommentReplies (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): CommentReplyPagination
`

export const Mutation = `
  createCommentReply (
    input: CommentReplyInput!
  ): CommentReply
`