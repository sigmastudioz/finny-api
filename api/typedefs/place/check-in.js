const constants = {
  Query: `
    _id: ObjectID
    place: Place
    user: ObjectID
    PlacePhoto: PlacePhoto
    PlacePhotos: PlacePhotoPagination
    title: String
    comment: String
    geometry: JSON
    createdAt: Date
    updatedAt: Date
    checked_in_with_friends: [SimpleProfile]
  `,
  Mutation: `
    place_id: String!
    user: ObjectID!
    place_photo: [ObjectID]
    title: String
    comment: String
    geometry: JSON!
  `
}

export default `
  type CheckIn {
    ${constants.Query}
    AddedToPlace (
      query: JSON
    ): Boolean
  }

  type CheckInPagination {    
    key: String
    docs: [CheckIn]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input CheckInInput {
    ${constants.Mutation}
  }
`

export const Query = `
  CheckIn (query: JSON): CheckIn
  CheckIns (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): CheckInPagination
`

export const Mutation = `
  createCheckIn (
    input: CheckInInput
  ): CheckIn

  updateCheckIn (
    _id: ObjectID!
    query: JSON
  ): CheckIn

  deleteCheckIn (
    _id: ObjectID!
  ): CheckIn
`