const constants = {
  Query: `
    user: ObjectID
    place: ObjectID
  `,
  Mutation: `
    category: ObjectID!
    user: ObjectID!
    place: ObjectID!
  `
}

export default `
  type CategoryList {
    _id: ObjectID
    ${constants.Query}
    createdAt: Date
    updatedAt: Date
  }

  type CategoryListPagination {  
    key: String  
    docs: [CategoryList]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input CategoryListInput {
    ${constants.Mutation}
  }
`

export const Query = `
  CategoryList (query: JSON): CategoryList
  CategoryLists (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): CategoryListPagination
`

export const Mutation = `
  createCategoryList (
    input: CategoryListInput!
  ): CategoryList

  updateCategoryList (
    _id: ObjectID!
    query: JSON
  ): CategoryList

  deleteCategoryList (
    _id: ObjectID!
  ): CategoryList
`