const constants = {
  Query: `
    user: ObjectID,
    place: ObjectID,
    tag: ObjectID,
    createdAt: Date,
    updatedAt: Date
  `
}
//  Tags: TagPagination
export default `
  type PlaceTag {
    _id: ObjectID
    ${constants.Query}
    Tags: TagPagination
  }

  type PlaceTagPagination {   
    key: String 
    docs: [PlaceTag]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input PlaceTagInput {
    ${constants.Query}
    user: ObjectID!
    place_id: ObjectID!
  }
`

export const Query = `
  PlaceTag (query: JSON): PlaceTag
  PlaceTags (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): PlaceTagPagination
`

export const Mutation = `
  createPlaceTag (
    input: PlaceTagInput!
  ): PlaceTag

  updatePlaceTag (
    _id: ObjectID!
    query: JSON
  ): PlaceTag

  deletePlaceTag (
    _id: ObjectID!
  ): PlaceTag
`