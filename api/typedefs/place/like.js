const constants = {
  Query: `
    user: ObjectID
    place_id: String!
  `
}

export default `
  type Like {
    _id: ObjectID
    ${constants.Query}
    createdAt: Date
    updatedAt: Date
  }

  type LikePagination { 
    key: String   
    docs: [Like]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input LikeInput {
    ${constants.Query}
  }
`

export const Query = `
  Like (query: JSON): Like
  Likes (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): LikePagination
`

export const Mutation = `
  createLike (
    input: LikeInput!
  ): Like

  updateLike (
    _id: ObjectID!
    query: JSON
  ): Like

  deleteLike (
    _id: ObjectID!
  ): Like
`