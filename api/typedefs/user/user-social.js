const constants = {
  Query: `
    local: UserSocialLocalType
    facebook: UserSocialFacebookType
    instagram: UserSocialInstagramType
    twitter: UserSocialTwitterType
    google: UserSocialGoogleType
    youtube: UserSocialYouTubeType
  `,
  Mutation: `
    local: UserSocialLocalInput
    facebook: UserSocialFacebookInput
    instagram: UserSocialInstagramInput
    twitter: UserSocialTwitterInput
    google: UserSocialGoogleInput
    youtube: UserSocialYouTubeInput
  `,
  Dates: `
    createdAt: Date
    updatedAt: Date
  `,
  SocialGeneral: `
    id: String
    token: String
  `,
  SocialCommon: `
    name: String
    profile: String
  `,
  LocalCommon: `
    email: String!
    changeEmail: String
    password: Password!
    resetPasswordToken: String
    resetPasswordExpires: String
    resetEmailToken: String
  `,
  TwitterCommon: `
    displayName: String
    username: String
  `
}
     
export default `
  type UserSocial {
    _id: ObjectID
    user: ObjectID
    token: String
    message: String
    ${constants.Query}
    ${constants.Dates}
  }

  input UserSocialInput {
    ${constants.Mutation}
  }

  type UserSocialLocalType {
    ${constants.LocalCommon}
  }

  input UserSocialLocalInput {
    ${constants.LocalCommon}
    username: String!
  }

  type UserSocialFacebookType {
    ${constants.SocialGeneral}
    ${constants.SocialCommon}
  }

  input UserSocialFacebookInput {
    ${constants.SocialGeneral}
    ${constants.SocialCommon}
  }

  type UserSocialTwitterType {
    ${constants.SocialGeneral}
    ${constants.SocialCommon}
    ${constants.TwitterCommon}
  }

  input UserSocialTwitterInput {
    ${constants.SocialGeneral}
    ${constants.TwitterCommon}
  }

  type UserSocialGoogleType {
    ${constants.SocialGeneral}
    ${constants.SocialCommon}
  }

  input UserSocialGoogleInput {
    ${constants.SocialGeneral}
    ${constants.SocialCommon}
  }

  type UserSocialYouTubeType {
    ${constants.SocialGeneral}
    ${constants.SocialCommon}
  }

  input UserSocialYouTubeInput {
    ${constants.SocialGeneral}
    ${constants.SocialCommon}
  }

  type UserSocialInstagramType {
    ${constants.SocialGeneral}
    ${constants.SocialCommon}
  }

  input UserSocialInstagramInput {
    ${constants.SocialGeneral}
    ${constants.SocialCommon}
  }

  type UserSocialPassword {
    status: Boolean
    message: String
    password: Password
    confirmPassword: Password
    currentPassword: String
  }

  input UserSocialPasswordInput {
    currentPassword: String
    password: Password
    confirmPassword: Password
  }


  type UserSocialPagination { 
    key: String   
    docs: [UserSocial]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }
`

export const Query = `
  UserLogin(query: LoginInput!): UserSocial
  UserSocial (query: UserQueryInput): UserSocial
  UserSocials (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): UserSocialPagination
`

export const Mutation = `
  updateUserSocial (
    _id: ObjectID!
    query: JSON
  ): UserSocial

  updateUserSocialPassword (
    input: UserSocialPasswordInput!
    query: JSON
  ): UserSocialPassword
`