const constants = {
  Query: `
    user: Profile
    friend: Profile
    message: String
    type: String
    createdAt: Date
    updatedAt: Date
  `
}

export default `
  type Notification {
    _id: ObjectID
    ${constants.Query}
  }

  type NotificationPagination {    
    key: String
    docs: [Notification]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }
`

export const Query = `
  Notification (query: JSON): Notification
  Notifications (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): NotificationPagination
`