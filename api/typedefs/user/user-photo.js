const constants = {
  Query: `
    url: String,
    key: String,
    user: ObjectID,
    type: String,
    createdAt: Date,
    updatedAt: Date
  `
}

export default `
  type UserPhoto {
    _id: ObjectID
    ${constants.Query}
    image: File
  }

  type UserPhotoPagination {  
    key: String  
    docs: [UserPhoto]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input UserPhotoInput {
    ${constants.Query}
    file: Upload!
  }
`

export const Query = `
  UserPhoto (query: JSON): UserPhoto
  UserPhotos (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): UserPhotoPagination
`

export const Mutation = `
  createUserPhoto (
    input: UserPhotoInput!
  ): UserPhoto

  updateUserPhoto (
    _id: ObjectID!
    query: JSON
  ): UserPhoto

  deleteUserPhoto (
    _id: ObjectID!
  ): UserPhoto
`