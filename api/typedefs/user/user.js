const constants = {
  Query: `
    userHash: ShortHash
    business: JSON
    username: String
    type: String
  `,
  Mutation: `
    profile: ProfileInput
    social: UserSocialInput
    setting: UserSettingInput
  `,
  Dates: `
    createdAt: Date
    updatedAt: Date
  `,
}
     
export default `
  type User {
    _id: ObjectID
    social: UserSocial
    profile: Profile
    setting: UserSetting
    ${constants.Query}
    ${constants.Dates}
  }

  input LoginInput {
    username: String!
    password: String!
  }

  input UserQueryInput {
    _id: ObjectID
    ${constants.Query}
    ${constants.Mutation}
  }

  type UserPagination { 
    key: String   
    docs: [User]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input UserInput {
    ${constants.Query}
    ${constants.Mutation}
  }
`

export const Query = `
  User (query: UserQueryInput): User
  Users (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): UserPagination
`

export const Mutation = `
  createUser (
    input: UserInput
  ): User

  updateUser (
    _id: ObjectID!
    query: JSON
  ): User

  deleteUser (
    _id: ObjectID!
  ): User
`