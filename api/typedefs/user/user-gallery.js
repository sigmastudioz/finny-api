const constants = {
  Query: `
    url: String,
    key: String,
    user: ObjectID,
    type: String,
    createdAt: Date,
    updatedAt: Date
  `
}

export default `
  type Gallery {
    _id: ObjectID
    ${constants.Query}
  }

  type GalleryPagination {   
    key: String 
    docs: [Gallery]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input GalleryInput {
    ${constants.Query}
  }
`

export const Query = `
  Gallery (query: JSON): Gallery
  Galleries (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): GalleryPagination
`

export const Mutation = `
  createGallery (
    input: GalleryInput!
  ): Gallery

  updateGallery (
    _id: ObjectID!
    query: JSON
  ): Gallery

  deleteGallery (
    _id: ObjectID!
  ): Gallery
`