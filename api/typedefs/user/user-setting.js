const constants = {
  Query: `
    verified: Boolean
    newProfile: Boolean
    delete: Boolean
    privacy: Boolean
    hidden: Boolean
  `
}

export default `
  type UserSetting {
    _id: ObjectID
    ${constants.Query}
    user: ObjectID
    notificationTypes: JSON
    privacyType: String
    deleteAt: Date
    createdAt: Date
    updatedAt: Date
  }

  type UserSettingPagination { 
    key: String   
    docs: [UserSetting]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input UserSettingInput {
    ${constants.Query}
    notificationTypes: JSON
    privacyType: String! = "public"
  }
`

export const Query = `
  UserSetting (query: JSON!): UserSetting
  UserSettings (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): UserSettingPagination
`

export const Mutation = `
  createUserSetting (
    input: UserSettingInput
  ): UserSetting

  updateUserSetting (
    _id: ObjectID!
    user: ObjectID!
    query: UserSettingInput
  ): UserSetting

  deleteUserSetting (
    _id: ObjectID!
  ): UserSetting
`