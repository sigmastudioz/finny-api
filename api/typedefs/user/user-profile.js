const constants = {
  Query: `
    middleName: String
    profileSummary: String
    dob: Date
    referer: String
  `
}

// gender: {
//   type: Schema.Types.ObjectId,
//   ref: 'user-gender',
//   default: null
// },
// avatar: {
//   type: Schema.Types.ObjectId,
//   ref: 'user-photo',
//   default: null
// },
// image: {
//   type: Schema.Types.ObjectId,
//   ref: 'user-photo',
//   default: null
// },

export default `
  type Profile {
    _id: ObjectID
    firstName: String
    lastName: String
    ${constants.Query}
    user: ObjectID
    Friend (query: JSON!): Friend
    Following (query: JSON): FriendPagination
    Followers (query: JSON): FriendPagination
    setting: UserSetting
    social: UserSocial
    createdAt: Date
    updatedAt: Date
  }

  type SimpleProfile {
    _id: ObjectID
    firstName: String
    lastName: String
  }

  type ProfilePagination {   
    key: String 
    docs: [SimpleProfile]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input ProfileInput {    
    firstName: String!
    lastName: String!
    user: ObjectID!
    ${constants.Query}
  }
`

export const Query = `
  Profile (query: JSON): Profile
  Profiles (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): ProfilePagination
`

export const Mutation = `
  createProfile (
    input: ProfileInput
  ): Profile

  updateProfile (
    _id: ObjectID!
    query: JSON
  ): Profile

  deleteProfile (
    _id: ObjectID!
  ): Profile
`