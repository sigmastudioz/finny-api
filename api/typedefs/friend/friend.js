const constants = {
  Query: `
    user: ObjectID
    friend: ObjectID
    approved: Boolean
    createdAt: Date
    updatedAt: Date
  `
}

// User: User

export default `
  type Friend {
    _id: ObjectID
    ${constants.Query}
    CheckIns(
      page: Int = 1
      limit: Int = 50
      query: JSON
      sortBy: String = "createdAt"
    ): CheckInPagination
  }

  type FriendPagination {
    key: String    
    docs: [Friend]
    total: Int!
    limit: Int!
    page: Int!
    pages: Int!
  }

  input FriendInput {
    user: ObjectID
    friend: ObjectID
  }
`

export const Query = `
  Friend (query: JSON): Friend
  Following (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): FriendPagination
  Followers (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): FriendPagination

  FriendCheckIns (
    page: Int = 1
    limit: Int = 50
    query: JSON
    sortBy: String = "createdAt"
  ): FriendPagination
`

export const Mutation = `
  createFriend (
    input: FriendInput!
  ): Friend

  updateFriend (
    _id: ObjectID!
    query: JSON
  ): Friend

  deleteFriend (
    query: FriendInput!
  ): Friend
`