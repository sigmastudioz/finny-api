class CommentResolver {
  constructor () {

  }

  async getComment (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getSingle({
        Model: Models.Comment,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `comment-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getComments (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null

      if (parent) {
        const place_id = parent.place_id
        DBQuery = {
          query: {
            place_id: {
              $eq: place_id.toString(),
            }
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.Comment,
        searchQuery: DBQuery,
        cacheKey: `comment-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createComment (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      const comment = await db.getSingle({
        Model: Models.Comment,
        searchQuery: {
          query: {
            user: ModelData.input.user,
            place_id: ModelData.input.place_id
          }
        }
      })

      if (comment && comment._id) {
        return comment
      } else {

        const postComment = await db.createSingle({
          Model: Models.Comment,
          ModelData: ModelData.input
        })

        await db.createSingle({
          Model: Models.UserNotification,
          ModelData: {
            message: 'has replied your post',
            friend: ModelData.input.user,
            user: ModelData.input.friend,
            mode : 'update',
            type: 'friend'
          }
        })
    
        await db.createSingle({
          Model: Models.UserNotification,
          ModelData: {
            message: 'replied a post',
            friend: ModelData.input.friend,
            user: ModelData.input.user,
            mode : 'update',
            type: 'friend'
          }
        })

        return postComment
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async updateComment (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.updateSingle({
        Model: Models.Comment,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `comment-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteComment (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.Comment,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `comment-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }
}

const Comment = new CommentResolver()

export const Resolvers = {
  Query: {
    Comment: Comment.getComment,
    Comments: Comment.getComments
  },
  SearchPlace: {
    Comments: Comment.getComments
  },
  Mutation: {
    createComment: Comment.createComment,
    updateComment: Comment.updateComment,
    deleteComment: Comment.deleteComment
  }
}