class PlaceTagResolver {
  constructor () {

  }

  async getPlaceTag (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getSingle({
        Model: Models.PlaceTag,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `place-tag-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getPlaceTags (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null

      if (parent) {
        const place_id = parent.place_id
        DBQuery = {
          query: {
            place_id: {
              $eq: place_id.toString(),
            }
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.PlaceTag,
        searchQuery,
        cacheKey: `place-tag-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createPlaceTag (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.createSingle({
        Model: Models.PlaceTag,
        ModelData: ModelData.input
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updatePlaceTag (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      
      return await db.updateSingle({
        Model: Models.PlaceTag,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `place-tag-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deletePlaceTag (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.PlaceTag,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `place-tag-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }
}

const PlaceTag = new PlaceTagResolver()

export const Resolvers = {
  Query: {
    PlaceTag: PlaceTag.getPlaceTag,
    PlaceTags: PlaceTag.getPlaceTags
  },
  Mutation: {
    createPlaceTag: PlaceTag.createPlaceTag,
    updatePlaceTag: PlaceTag.updatePlaceTag,
    deletePlaceTag: PlaceTag.deletePlaceTag
  }
}