class LikeResolver {
  constructor () {

  }

  async getLike (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      
      return await db.getSingle({
        Model: Models.Like,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `like-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getLikes (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let DBQuery = null

      if (parent) {
        const place_id = parent.place_id
        DBQuery = {
          query: {
            place_id: {
              $eq: place_id.toString(),
            }
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.Like,
        searchQuery: DBQuery,
        cacheKey: `likes-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createLike (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.createSingle({
        Model: Models.Like,
        ModelData: ModelData.input
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updateLike (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      db.Model = Models.Like

      return await db.updateSingle({
        Model: Models.Like,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `like-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteLike (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.Like,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `like-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }
}

const Like = new LikeResolver()

export const Resolvers = {
  Query: {
    Like: Like.getLike,
    Likes: Like.getLikes
  },
  SearchPlace: {
    Likes: Like.getLikes
  },
  Mutation: {
    createLike: Like.createLike,
    updateLike: Like.updateLike,
    deleteLike: Like.deleteLike
  }
}