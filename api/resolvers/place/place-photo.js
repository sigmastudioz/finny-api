import _ from 'lodash'
import { GOOGLE_ACCESS_KEY, SITE_ENV } from '../../../utils/constants'
import { shortIdGen } from '../../../utils/func-helpers'

class PlacePhotoResolver {
  constructor () {

  }

  async getPlacePhoto (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let DBQuery = null

      if (parent) {
        const place_id = parent.place_id
        DBQuery = {
          query: {
            place_id: {
              $eq: place_id.toString(),
            }
          }
        }
      } else {
        DBQuery = {
          query: {
            _id: searchQuery._id
          }
        }
      }

      return await db.getSingle({
        Model: Models.PlacePhoto,
        searchQuery: DBQuery,
        cacheKey: `photo-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getPlacePhotos (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let DBQuery = null

      if (parent) {
        const place_id = parent.place_id
        DBQuery = {
          query: {
            place_id: {
              $eq: place_id.toString(),
            }
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.PlacePhoto,
        searchQuery: DBQuery,
        cacheKey: `photo-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createPlacePhoto (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo,
    s3
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.createSingle({
        Model: Models.PlacePhoto,
        ModelData: ModelData.input
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updatePlacePhoto (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.updateSingle({
        Model: Models.PlacePhoto,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `photo-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deletePlacePhoto (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.PlacePhoto,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `photo-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }


  async getMapsPlacesPhoto (parent, searchQuery, {
    GoogleMaps,
    CacheClient,
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const cache = new CacheClient()
      let cacheKey = null
      let DBQuery = null

      if (parent) {
        DBQuery = {
          maxwidth: 1600,
          photoreference: parent.photos[0].photo_reference
        }
      } else {
        DBQuery = searchQuery.query
      }

      let cacheResults = await cache.getCache({
        cacheKey: `gmaps-search-place-id-photo-${JSON.stringify(DBQuery)}`
      })
      if (cacheResults.status) {
        return {
          nextpagetoken: cacheResults.data.next_page_token,
          docs: cacheResults.data.results
        }
      } else {
        const getPlacesPhoto = await GoogleMaps.getPlacesPhoto(DBQuery)   
        // if (getPlaces.status) {
        //   cacheResults = await cache.setCache({
        //     cacheKey,
        //     cacheData: getPlaces
        //   })
        //   return {
        //     nextpagetoken: cacheResults.data.next_page_token,
        //     docs: cacheResults.data.results
        //   }
        // } else {
        //   return {
        //     nextpagetoken: getPlaces.next_page_token,
        //     docs: getPlaces.results
        //   }
        // }

        // return getPlacesPhoto
        return {}
      } 
    } catch (e) {
      return new Error(e)
    }   
  }

  async Photos (parent, searchQuery, {
    GoogleMaps,
    CacheClient,
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      return _.map(parent.photos || [], (photo) => {
        return {
          url : photo.photo_reference? `https://maps.googleapis.com/maps/api/place/photo?maxwidth=1600&photoreference=${photo.photo_reference}&key=${GOOGLE_ACCESS_KEY}` : null
        }
      })
    } catch (e) {
      return new Error(e)
    }   
  }
}

const PlacePhoto = new PlacePhotoResolver()

export const Resolvers = {
  Query: {
    PlacePhoto: PlacePhoto.getPlacePhoto,
    PlacePhotos: PlacePhoto.getPlacePhotos
  },
  CheckIn: {
    PlacePhoto: PlacePhoto.getPlacePhoto,
    PlacePhotos: PlacePhoto.getPlacePhotos
  },
  SearchPlace: {
    UsersPhotos: PlacePhoto.getPlacePhotos,
    PlacePhotos: PlacePhoto.getMapsPlacesPhoto,
    photos: PlacePhoto.Photos
  },
  Mutation: {
    createPlacePhoto: PlacePhoto.createPlacePhoto,
    updatePlacePhoto: PlacePhoto.updatePlacePhoto,
    deletePlacePhoto: PlacePhoto.deletePlacePhoto
  }
}