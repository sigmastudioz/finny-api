import _ from 'lodash'
import moment from 'moment'
class CheckInResolver {
  constructor () {

  }

  async getCheckIn (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null
      let cacheKey = null

      if (parent) {
        DBQuery = {
          query: {
            $or:[{
              place_id: parent.place_id
            }]
          }
        }
        cacheKey = `check-in-id-${parent.place_id}`
      } else {
        DBQuery = searchQuery
        cacheKey = `check-in-id-${JSON.stringify(DBQuery)}`
      }
      
      return await db.getSingle({
        Model: Models.CheckIn,
        searchQuery: DBQuery,
        cacheKey
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getCheckIns (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let query = null
      let DBQuery = null
      
      if (parent) {
        const place_id = parent.place_id
        query = searchQuery.query || {}
        DBQuery = JSON.parse(JSON.stringify({
          query: {
            $and: _.merge([{
              place_id: {
                $eq: place_id.toString(),
              }
            }], [query])
          }
        }))
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.CheckIn,
        searchQuery: DBQuery,
        cacheKey: `check-ins-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createCheckIn (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let checkin = await db.getSingle({
        Model: Models.CheckIn,
        searchQuery: {
          query: {
            user: ModelData.input.user,
            place_id: ModelData.input.place_id,
            createdAt: {
              $gte: `${moment().startOf('day').toDate()}`,
              $lt: `${moment().endOf('day').toDate()}`
            }
          }
        }
      })

      

      if (!checkin.status) {
        checkin = await db.createSingle({
          Model: Models.CheckIn,
          ModelData: ModelData.input
        })

        

        if(checkin && checkin._id) {
          await db.updateBulk({
            Model: Models.Friend,
            searchQuery: {
              query: {
                friend: ModelData.input.user,
              }
            },
            ModelData: {}
          })
        }
      }
      return checkin
    } catch (e) {
      return new Error(e)
    }
  }

  async updateCheckIn (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      db.Model = Models.CheckIn

      return await db.updateSingle({
        Model: Models.CheckIn,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `check-in-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteCheckIn (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.CheckIn,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `check-in-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }


  async recentCheckIn (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null
      let cacheKey = null

      if (parent) {
        DBQuery = {
          query: {
            $or:[{
              place_id: parent.place_id
            }]
          }
        }
        cacheKey = `check-in-id-${parent.place_id}`
      } else {
        DBQuery = searchQuery
        cacheKey = `check-in-id-${JSON.stringify(DBQuery)}`
      }
      
      const { status } = await db.getSingle({
        Model: Models.CheckIn,
        searchQuery: DBQuery,
        cacheKey
      })

      return status
    } catch (e) {
      return new Error(e)
    }
  }

  async getUserCheckIns (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let DBQuery = null
      
      if (parent) {
        
        const user = parent.friend || parent.user
        DBQuery = JSON.stringify(_.merge({
          page: searchQuery.page,
          limit: searchQuery.limit,
          sortBy: searchQuery.sortBy,
          query: {
            user: {
              $eq: user
            }
          }
        }))

        if (searchQuery.query && searchQuery.query.createdAt) {
          DBQuery = JSON.stringify(_.merge(DBQuery, {
            query: {
              createdAt: searchQuery.query.createdAt || null
            }
          }))
        }

        if (searchQuery.query && searchQuery.query.updatedAt) {
          DBQuery = JSON.stringify(_.merge(DBQuery, {
            query: {
              updatedAt: searchQuery.query.updatedAt || null
            }
          }))
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.CheckIn,
        searchQuery: DBQuery,
        cacheKey: `check-ins-by-user-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }
}

const CheckIn = new CheckInResolver()

export const Resolvers = {
  Query: {
    CheckIn: CheckIn.getCheckIn,
    CheckIns: CheckIn.getCheckIns
  },
  SearchPlace: {
    CheckedIn: CheckIn.recentCheckIn,
    UserCheckIns: CheckIn.getCheckIns
  },
  Place: {
    CheckIns: CheckIn.getUserCheckIns
  },
  Friend: {
    CheckIns: CheckIn.getUserCheckIns
  },
  Mutation: {
    createCheckIn: CheckIn.createCheckIn,
    updateCheckIn: CheckIn.updateCheckIn,
    deleteCheckIn: CheckIn.deleteCheckIn
  }
}