class TagResolver {
  constructor () {

  }

  async getTag (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getSingle({
        Model: Models.Tag,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `tag-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getTags (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let DBQuery = null

      if (parent) {
        DBQuery = {
          query: {
            place_id: parent.place_id
          }
        }
      } else {
        DBQuery = searchQuery
      }
      return await db.getCollection({
        Model: Models.Tag,
        searchQuery,
        cacheKey: `tag-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createTag (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.createSingle({
        Model: Models.Tag,
        ModelData: ModelData.input
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updateTag (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      
      return await db.updateSingle({
        Model: Models.Tag,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `tag-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteTag (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.Tag,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `tag-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }
}

const Tag = new TagResolver()

export const Resolvers = {
  Query: {
    Tag: Tag.getTag,
    Tags: Tag.getTags
  },
  Place: {
    Tags: Tag.getTags
  },
  SearchPlace: {
    Tags: Tag.getTags
  },
  Mutation: {
    createTag: Tag.createTag,
    updateTag: Tag.updateTag,
    deleteTag: Tag.deleteTag
  }
}