import _ from 'lodash'

class CategoryResolver {
  constructor () {

  }

  async getCategory (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let DBQuery = null

      if (parent) {
        DBQuery = {
          query: {
            $and:[{
              place_id: parent.place_id
            },
            {
              user: parent.user
            }]
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getSingle({
        Model: Models.Category,
        searchQuery: DBQuery,
        cacheKey: `category-id-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getCategories (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getCollection({
        Model: Models.Category,
        searchQuery,
        cacheKey: `category-by-page-${JSON.stringify(searchQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createCategory (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let categoryList = []
      const category = await db.createSingle({
        Model: Models.Category,
        ModelData: ModelData.input
      })

      if ( ModelData.input &&  ModelData.input.Lists ) {
        _.each(ModelData.input.Lists, (list) => {
          categoryList.push(db.createSingle({
            Model: Models.List,
            ModelData:  _.merge(list, {
              category: category._id,
              user: category.user
            })
          }))
        })
        return await Promise.all(categoryList).then((results) => {
          category.Lists = results
          return category
        }).catch((e) => {
          console.log(e)
        })
      } else {
        return category
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async updateCategory (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let categoryList = []
      const category = await db.updateSingle({
        Model: Models.Category,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `category-id-${searchQuery._id}`
      })

      if ( searchQuery.query &&  searchQuery.query.Lists ) {
        _.each(searchQuery.query.Lists, (list) => {
          categoryList.push(db.createSingle({
            Model: Models.List,
            ModelData:  _.merge(list, {
              category: category._id,
              user: category.user
            })
          }))
        })
        return await Promise.all(categoryList).then((results) => {
          category.Lists = results
          return category
        }).catch((e) => {
          console.log(e)
        })
      } else {
        return category
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteCategory (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      const category = await db.deleteSingle({
        Model: Models.Category,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `category-id-${searchQuery._id}`
      })

      db.deleteBulk({
        Model: Models.List,
        searchQuery: {
          query: {
            category: category._id,
            user: category.user
          }
        }
      }).then((results) => {
        console.log(results)
      }).catch((e) => {
        console.log(e)
      })

      return category
    } catch (e) {
      return new Error(e)
    }
  }
}

const Category = new CategoryResolver()

export const Resolvers = {
  Query: {
    Category: Category.getCategory,
    Categories: Category.getCategories
  },
  Mutation: {
    createCategory: Category.createCategory,
    updateCategory: Category.updateCategory,
    deleteCategory: Category.deleteCategory
  }
}