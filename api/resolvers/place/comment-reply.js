class CommentReplyResolver {
  constructor () {

  }

  async getCommentReply (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getSingle({
        Model: Models.CommentReply,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `comment-reply-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getCommentReplies (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null

      if (parent) {
        const place_id = parent.place_id
        const user = parent.user
        DBQuery = {
          query: {
            place_id: {
              $eq: place_id.toString()
            },
            friend: {
              $eq: user.toString()
            }
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.CommentReply,
        searchQuery: DBQuery,
        cacheKey: `comment-replie-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createCommentReply (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      const commentReply = await db.createSingle({
        Model: Models.CommentReply,
        ModelData: ModelData.input
      })

      await db.createSingle({
        Model: Models.UserNotification,
        ModelData: {
          message: 'has replied your comment',
          friend: ModelData.input.user,
          user: ModelData.input.friend,
          mode : 'update',
          type: 'friend'
        }
      })
  
      await db.createSingle({
        Model: Models.UserNotification,
        ModelData: {
          message: 'replied a comment',
          friend: ModelData.input.friend,
          user: ModelData.input.user,
          mode : 'update',
          type: 'friend'
        }
      })

      return commentReply
    } catch (e) {
      return new Error(e)
    }
  }
}

const CommentReply = new CommentReplyResolver()

export const Resolvers = {
  Query: {
    CommentReply: CommentReply.getCommentReply,
    CommentReplies: CommentReply.getCommentReplies
  },
  Comment: {
    Replies: CommentReply.getCommentReplies
  },
  Mutation: {
    createCommentReply: CommentReply.createCommentReply
  }
}