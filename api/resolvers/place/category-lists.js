import _ from 'lodash'

class CategoryListResolver {
  constructor () {

  }

  async getCategoryList (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let DBQuery = null

      if (parent) {
        DBQuery = {
          query: {
            $and:[{
              place_id: parent.place_id
            },
            {
              category: parent.category
            },
            {
              user: parent.user
            }]
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getSingle({
        Model: Models.CategoryList,
        searchQuery: DBQuery,
        cacheKey: `category-lists-id-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getCategoryLists (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getCollection({
        Model: Models.CategoryList,
        searchQuery,
        cacheKey: `category-lists-by-page-${JSON.stringify(searchQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createCategoryList (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let categoryListList = []
      const categoryList = await db.createSingle({
        Model: Models.CategoryList,
        ModelData: ModelData.input
      })

      if ( ModelData.input &&  ModelData.input.Lists ) {
        _.each(ModelData.input.Lists, (list) => {
          categoryListList.push(db.createSingle({
            Model: Models.CategoryList,
            ModelData:  _.merge(list, {
              categoryList: categoryList._id,
              user: categoryList.user
            })
          }))
        })
        return await Promise.all(categoryListList).then((results) => {
          categoryList.Lists = results
          return categoryList
        }).catch((e) => {
          console.log(e)
        })
      } else {
        return categoryList
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async updateCategoryList (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let categoryListList = []
      const categoryList = await db.updateSingle({
        Model: Models.CategoryList,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `category-list-id-${searchQuery._id}`
      })

      if ( searchQuery.query &&  searchQuery.query.Lists ) {
        _.each(searchQuery.query.Lists, (list) => {
          categoryListList.push(db.createSingle({
            Model: Models.CategoryList,
            ModelData:  _.merge(list, {
              categoryList: categoryList._id,
              user: categoryList.user
            })
          }))
        })
        return await Promise.all(categoryListList).then((results) => {
          categoryList.Lists = results
          return categoryList
        }).catch((e) => {
          console.log(e)
        })
      } else {
        return categoryList
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteCategoryList (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      const categoryList = await db.deleteSingle({
        Model: Models.CategoryList,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `category-list-id-${searchQuery._id}`
      })

      db.deleteBulk({
        Model: Models.CategoryList,
        searchQuery: {
          query: {
            categoryList: categoryList._id,
            user: categoryList.user
          }
        }
      }).then((results) => {
        console.log(results)
      }).catch((e) => {
        console.log(e)
      })

      return categoryList
    } catch (e) {
      return new Error(e)
    }
  }
}

const CategoryList = new CategoryListResolver()

export const Resolvers = {
  Query: {
    CategoryList: CategoryList.getCategoryList,
    CategoryLists: CategoryList.getCategoryLists
  },
  Place: {
    addedToCategory: CategoryList.getCategoryList
  },
  Mutation: {
    createCategoryList: CategoryList.createCategoryList,
    updateCategoryList: CategoryList.updateCategoryList,
    deleteCategoryList: CategoryList.deleteCategoryList
  }
}