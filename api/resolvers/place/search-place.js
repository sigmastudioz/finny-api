import ms from 'milliseconds'

class SearchPlaceResolver {
  constructor () {

  }

  async searchPlaces (parent, searchQuery, {
    GoogleMaps,
    CacheClient,
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      // console.log(JSON.stringify(searchQuery))
      const cache = new CacheClient()
      const cacheKey = `gmaps-search-place-by-page-${JSON.stringify(searchQuery)}`

      let cacheResults = await cache.getCache({
        cacheKey
      })
      if (cacheResults.status) {
        return {
          nextpagetoken: cacheResults.data.next_page_token,
          docs: cacheResults.data.results
        }
      } else {
        const getPlaces = await GoogleMaps.getPlaces(searchQuery.query)
        if (getPlaces.status === 'OK') {
          cacheResults = await cache.setCache({
            cacheKey,
            cacheData: getPlaces
          })
          return {
            nextpagetoken: cacheResults.data.next_page_token,
            docs: cacheResults.data.results
          }
        } else {
          return {
            nextpagetoken: getPlaces.next_page_token,
            docs: getPlaces.results
          }
        }
      } 
    } catch (e) {
      return new Error(e)
    }   
  }

  async getPlace (parent, searchQuery, {
    GoogleMaps,
    CacheClient,
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const cache = new CacheClient()
      let DBQuery = null

      if (parent) {
        DBQuery = {
          placeid: parent.place_id
        }
      } else {
        DBQuery = searchQuery.query
      }

      const cacheKey = `gmaps-search-place-by-id-${JSON.stringify(DBQuery.placeid)}`

      let cacheResults = await cache.getCache({
        cacheKey
      })

      if (cacheResults.status) {
        return cacheResults.data
      } else {
        const getPlace = await GoogleMaps.getPlace(DBQuery)   
        if (getPlace.status === 'OK') {
          cacheResults = await cache.setCache({
            cacheKey,
            cacheData: getPlace.result,
            expire: ms.weeks(1) * 0.001
          })
          return cacheResults.data
        } else {
          return  getPlace.result
        }
      } 
    } catch (e) {
      return new Error(e)
    }   
  }
}

const SearchPlace = new SearchPlaceResolver()

export const Resolvers = {
  Query: {
    SearchPlaces: SearchPlace.searchPlaces,
    GetPlace: SearchPlace.getPlace
  },
  CheckIn: {
    place: SearchPlace.getPlace
  }
}