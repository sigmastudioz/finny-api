import _ from 'lodash'
class PlaceResolver {
  constructor () {

  }

  async getPlace (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo,
    Helpers
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null

      if (parent) {
        let queryOpts = []
        if (parent._id) {
          queryOpts.push({
            category: {
              $eq: parent._id
            }
          })
        }

        if (parent.place_id) {
          queryOpts.push({
            place_id: {
              $eq: parent.place_id
            }
          })
        }

        if (parent.user) {
          queryOpts.push({
            user: {
              $eq: parent.user
            }
          })
        }

        DBQuery = JSON.stringify(({
          query: {
            $or: queryOpts
          }
        }))
      } else {
        DBQuery = {
          query: {
            _id: searchQuery._id
          }
        }
      }

      return await db.getSingle({
        Model: Models.Place,
        searchQuery: DBQuery,
        cacheKey: `place-id-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getPlaces (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo,
    Helpers
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null
      if (parent) {
        let queryOpts = []
        if (parent._id) {
          queryOpts.push({
            category: {
              $eq: parent._id
            }
          })
        }

        if (parent.place_id) {
          queryOpts.push({
            place_id: {
              $eq: parent.place_id
            }
          })
        }

        if (parent.user) {
          queryOpts.push({
            user: {
              $eq: parent.user
            }
          })
        }

        DBQuery = _.merge({
          query: {
            $or: queryOpts
          }
        })
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.Place,
        searchQuery: DBQuery,
        cacheKey: `place-by-page-${JSON.stringify(DBQuery)}`
      })

    } catch (e) {
      return new Error(e)
    }
  }

  async createPlace (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      const savedPlace = await db.getSingle({
        Model: Models.Place,
        searchQuery: {
          query: {
            user: ModelData.input.user,
            place_id: ModelData.input.place_id
          }
        }
      })

      if (savedPlace && savedPlace._id) {
        return savedPlace
      } else {
        return await db.createSingle({
          Model: Models.Place,
          ModelData: ModelData.input
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async updatePlace (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.updateSingle({
        Model: Models.Place,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `place-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deletePlace (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.Place,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `place-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async addedToPlace (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo,
    Helpers
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null

      if (parent) {
        // console.log(parent)
        let queryOpts = []
        // if (parent._id) {
        //   queryOpts.push({
        //     category: {
        //       $eq: parent._id
        //     }
        //   })
        // }

        if (parent.place_id) {
          queryOpts.push({
            place_id: {
              $eq: parent.place_id
            }
          })
        }

        // if (parent.user) {
        //   queryOpts.push({
        //     user: {
        //       $eq: parent.user
        //     }
        //   })
        // }

        DBQuery = JSON.stringify(({
          query: {
            $or: queryOpts
          }
        }))
      } else {
        DBQuery = {
          query: {
            _id: searchQuery._id
          }
        }
      }

      const { status }  = await db.getSingle({
        Model: Models.Place,
        searchQuery: DBQuery,
        cacheKey: `place-id-${JSON.stringify(DBQuery)}`
      })

      return status
    } catch (e) {
      return new Error(e)
    }
  }
}

const Place = new PlaceResolver()

export const Resolvers = {
  Query: {
    Place: Place.getPlace,
    Places: Place.getPlaces
  },
  CheckIn: {
    AddedToPlace: Place.addedToPlace
  },
  SearchPlace: {
    AddedToPlace: Place.addedToPlace
  },
  Category: {
    Places: Place.getPlaces
  },
  Mutation: {
    createPlace: Place.createPlace,
    updatePlace: Place.updatePlace,
    deletePlace: Place.deletePlace
  }
}