class CommentFlagResolver {
  constructor () {

  }

  async getCommentFlag (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getSingle({
        Model: Models.CommentFlag,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `comment-flag-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getCommentFlags (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null

      if (parent) {
        const place_id = parent.place_id
        DBQuery = {
          query: {
            place_id: {
              $eq: place_id.toString(),
            },
            $or: [
              { comment: parent._id },
              { reply: parent._id }
            ]
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.CommentFlag,
        searchQuery: DBQuery,
        cacheKey: `comment-flag-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createCommentFlag (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      const comment = await db.getSingle({
        Model: Models.Comment,
        searchQuery: {
          query: {
            user: ModelData.input.user,
            place_id: ModelData.input.place_id
          }
        }
      })

      if (comment && comment._id) {
        return comment
      } else {
        return await db.createSingle({
          Model: Models.CommentFlag,
          ModelData: ModelData.input
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }
}

const CommentFlag = new CommentFlagResolver()

export const Resolvers = {
  Query: {
    CommentFlag: CommentFlag.getCommentFlag,
    CommentFlags: CommentFlag.getCommentFlags
  },
  Comment: {
    Flags: CommentFlag.getCommentFlags
  },
  CommentReply: {
    Flags: CommentFlag.getCommentFlags
  },
  Mutation: {
    createCommentFlag: CommentFlag.createCommentFlag
  }
}