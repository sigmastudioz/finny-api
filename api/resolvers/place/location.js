class LocationResolver {
  constructor () {

  }

  async getLocation (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getSingle({
        Model: Models.Location,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `location-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getLocations (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getCollection({
        Model: Models.Location,
        searchQuery,
        cacheKey: `location-by-page-${JSON.stringify(searchQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createLocation (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.createSingle({
        Model: Models.Location,
        ModelData: ModelData.input
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updateLocation (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.updateSingle({
        Model: Models.Location,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `location-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteLocation (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      
      return await db.deleteSingle({
        Model: Models.Location,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `location-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }
}

const Location = new LocationResolver()

export const Resolvers = {
  Query: {
    Location: Location.getLocation,
    Locations: Location.getLocations
  },
  Mutation: {
    createLocation: Location.createLocation,
    updateLocation: Location.updateLocation,
    deleteLocation: Location.deleteLocation
  }
}