class UserSettingResolver {
  constructor () {

  }

  async getUserSetting (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null
      let cacheKey = null

      if (parent) {
        DBQuery = {
          query: {
            $or:[{
              _id: parent.setting
            },{
              user: parent.user
            }]
          }
        }
        cacheKey = `user-setting-id-${parent.setting || parent.user}`
      } else {
        DBQuery = searchQuery
        cacheKey = `user-setting-id-${JSON.stringify(DBQuery)}`
      }

      return await db.getSingle({
        Model: Models.UserSetting,
        searchQuery: DBQuery,
        cacheKey
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getUserSettings (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getCollection({
        Model: Models.UserSetting,
        searchQuery,
        cacheKey: `user-setting-by-page-${JSON.stringify(searchQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createUserSetting (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.createSingle({
        Model: Models.UserSetting,
        ModelData: ModelData.input
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updateUserSetting (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.updateSingle({
        Model: Models.UserSetting,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id,
            user: searchQuery.user
          }
        },
        cacheKey: `user-setting-id-${searchQuery.user}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteUserSetting (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.UserSetting,
        searchQuery: {
          query: {
            user: searchQuery.user
          }
        },
        cacheKey: `user-setting-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }
}

const UserSetting = new UserSettingResolver()

export const Resolvers = {
  Query: {
    UserSetting: UserSetting.getUserSetting,
    UserSettings: UserSetting.getUserSettings
  },
  User: {
    setting: UserSetting.getUserSetting
  },
  Profile: {
    setting: UserSetting.getUserSetting
  },
  Mutation: {
    createUserSetting: UserSetting.createUserSetting,
    updateUserSetting: UserSetting.updateUserSetting,
    deleteUserSetting: UserSetting.deleteUserSetting
  }
}