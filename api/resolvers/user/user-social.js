import _ from 'lodash'

class UserSocialResolver {
  constructor () {

  }

  async getUserSocial (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null

      if (parent) {
        DBQuery = {
          query: {
            $or:[{
              user: parent.user
            }]
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getSingle({
        Model: Models.UserSocial,
        searchQuery: DBQuery,
        cacheKey: `user-social-id-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getUserSocials (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getCollection({
        Model: Models.UserSocial,
        searchQuery,
        cacheKey: `user-socials-by-page-${JSON.stringify(searchQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createUserSocial (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.createSingle({
        Model: Models.UserSocial,
        ModelData: ModelData.input
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updateUserSocial (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.updateSingle({
        Model: Models.UserSocial,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `user-social-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteUserSocial (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.UserSocial,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `user-social-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async loginUser (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo,
    Helpers
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      const userLogin = await db.getSingle({
        Model: Models.UserSocial,
        searchQuery: {
          query: {
            'local.username': searchQuery.query.username
          }
        }
      })
      
      if (userLogin && userLogin._id) {
        const validPassword = await Helpers.validPassword({ 
          CurrentPassword: searchQuery.query.password, 
          DbPassword: userLogin.local.password 
        })
    
        if (validPassword && validPassword.status ){
          const AuthTokenize = await Helpers.AuthTokenize(userLogin)

          return _.merge(AuthTokenize, {
            user: userLogin.user
          })
        } else {
          return validPassword
        }
      } else {
        return {
          success: false,
          message: 'User not found!'
        }
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async updateUserSocialPassword (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo,
    Helpers
  }) {

    return {
      status: true
    }
  }
}

const UserSocial = new UserSocialResolver()

export const Resolvers = {
  Query: {
    UserSocial: UserSocial.getUserSocial,
    UserSocials: UserSocial.getUserSocials,
    UserLogin: UserSocial.loginUser
  },
  User: {
    social: UserSocial.getUserSocial
  },
  Profile: {
    social: UserSocial.getUserSocial
  },
  Mutation: {
    updateUserSocial: UserSocial.updateUserSocial,
    updateUserSocialPassword: UserSocial.updateUserSocialPassword
  }
}