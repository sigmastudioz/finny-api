class UserPhotoResolver {
  constructor () {

  }

  async getUserPhoto (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getSingle({
        Model: Models.UserPhoto,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `photo-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getUserPhotos (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getCollection({
        Model: Models.UserPhoto,
        searchQuery,
        cacheKey: `photo-by-page-${JSON.stringify(searchQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createUserPhoto (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.createSingle({
        Model: Models.UserPhoto,
        ModelData: ModelData.input
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updateUserPhoto (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.updateSingle({
        Model: Models.UserPhoto,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `photo-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteUserPhoto (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.UserPhoto,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `photo-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }
}

const UserPhoto = new UserPhotoResolver()

export const Resolvers = {
  Query: {
    UserPhoto: UserPhoto.getUserPhoto,
    UserPhotos: UserPhoto.getUserPhotos
  },
  // Galleries: {
  //   UserPhotos: UserPhoto.getUserPhotos
  // },
  Mutation: {
    createUserPhoto: UserPhoto.createUserPhoto,
    updateUserPhoto: UserPhoto.updateUserPhoto,
    deleteUserPhoto: UserPhoto.deleteUserPhoto
  }
}