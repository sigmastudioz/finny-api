class GalleryResolver {
  constructor () {

  }

  async getGallery (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getSingle({
        Model: Models.Gallery,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `gallery-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getGalleries (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    const db = new DBCacheHelpers()
    db.cache = DbInfo.cache
    db.db = DbInfo.db

    return await db.getCollection({
      Model: Models.Gallery,
      searchQuery,
      cacheKey: `gallery-by-page-${JSON.stringify(searchQuery)}`
    })
  }

  async createGallery (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    const db = new DBCacheHelpers()
    db.cache = DbInfo.cache
    db.db = DbInfo.db

    return await db.createSingle({
      Model: Models.Gallery,
      ModelData: ModelData.input
    })
  }

  async updateGallery (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    const db = new DBCacheHelpers()
    db.cache = DbInfo.cache
    db.db = DbInfo.db

    return await db.updateSingle({
      Model: Models.Gallery,
      ModelData: searchQuery.query,
      searchQuery: {
        query: {
          _id: searchQuery._id
        }
      },
      cacheKey: `gallery-id-${searchQuery._id}`
    })
  }

  async deleteGallery (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    const db = new DBCacheHelpers()
    db.cache = DbInfo.cache
    db.db = DbInfo.db

    return await db.deleteSingle({
      Model: Models.Gallery,
      searchQuery: {
        query: {
          _id: searchQuery._id
        }
      },
      cacheKey: `gallery-id-${searchQuery._id}`
    })
  }
}

const Gallery = new GalleryResolver()

export const Resolvers = {
  Query: {
    Gallery: Gallery.getGallery,
    Galleries: Gallery.getGalleries
  },
  // Place: {
  //   Galleries: Gallery.getGalleries
  // },
  Mutation: {
    createGallery: Gallery.createGallery,
    updateGallery: Gallery.updateGallery,
    deleteGallery: Gallery.deleteGallery
  }
}