
import _ from 'lodash'
class UserResolver {
  constructor () {

  }

  async getUser (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      
      return await db.getSingle({
        Model: Models.User,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `user-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getUsers (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let DBQuery = null

      if (parent) {
        const place_id = parent.place_id
        DBQuery = {
          query: {
            place_id: {
              $eq: place_id.toString(),
            }
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.User,
        searchQuery: DBQuery,
        cacheKey: `users-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createUser (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo,
    EmailSend
  }) {
    try {
      let userID = null
      let socialID = null
      let profileID = null
      let imageID = null
      let settingID = null
      let db = null

      db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      // console.log(ModelData)
      // return ModelData
      let userData = ModelData.input
      const userName = userData.social.local.username
      const email = userData.social.local.email
      
      const userLogin = await db.getSingle({
        Model: Models.UserSocial,
        searchQuery: {
          query: {
            'local.username': userName
          }
        }
      })
      
      if (userLogin && userLogin._id) {
        return {
          _id: userLogin.user
        }
      } else {
        userID = await db.createSingle({
          Model: Models.User
        })

        if (userData && userData.profile && userData.profile.image) {
          imageID = await db.createSingle({
            Model: Models.UserPhoto,
            ModelData: _.merge(userData.profile.image, {
              user: userID._id
            })
          })
          delete userData.profile.image
        }

        if (userData && userData.profile) {
          profileID = await db.createSingle({
            Model: Models.UserProfile,
            ModelData: _.merge(userData.profile, {
              image: (imageID && imageID._id) || null,
              user: userID._id
            })
          })

          delete userData.profile
        }

        if (userData && userData.social) {
          socialID = await db.createSingle({
            Model: Models.UserSocial,
            ModelData: _.merge(userData.social, {
              user: userID._id,
            })
          })
          delete userData.social
        }

        settingID = await db.createSingle({
          Model: Models.UserSetting,
          ModelData: _.merge(userData.setting || {}, {
            user: userID._id
          })
        })

        const updatedData = _.merge(userData, {
          profile: (profileID && profileID._id) || null,
          social: (socialID && socialID._id) || null,
          setting: (settingID && settingID._id) || null
        })

        const updatedUser = await db.updateSingle({
          Model: Models.User, 
          ModelData: updatedData,
          searchQuery: {
            query: {
              _id: userID._id
            }
          },
          cacheKey: `user-id-${userID._id}`
        })

        EmailSend.sendMail({
          text:	`i hope this works`,
          from:	`Finny <no-reply@finny-app.com>`,
          to:		`${userName} <${email}>`,
          subject:	`Welcome to FINNY`,
          attachment:
          [
            { data: `<html>
                Welcome ${userName}, <br> 
                <p>
                  You have been registered!<br>
                  Search and create lists for your everyday organizer <br>
                  to organize your everyday.<br>
                  <i>Resturants * Bars * Cafes</i>
                </p>
                <p>
                  Cheers,<br>
                  <strong>FINNY</strong>
                </p>
              </html>
            `, alternative: true }
          ]
       })

        return updatedUser
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async updateUser (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      let db = null
      db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      // let userData = searchQuery.input

      // if (userData && userData.social) {
      //   await db.updateSingle({
      //     Model: Models.UserSocial,
      //     ModelData: userData.social,
      //     searchQuery: {
      //       query: {
      //         _id: userData.social._id
      //       }
      //     },
      //     cacheKey: `social-id-${profile.image._id}`
      //   })
      //   delete userData.social
      // }

      // if (userData && userData.setting) {
      //   await db.updateSingle({
      //     Model: Models.UserSetting,
      //     ModelData: userData.setting,
      //     searchQuery: {
      //       query: {
      //         _id: userData.setting._id
      //       }
      //     },
      //     cacheKey: `setting-id-${profile.image._id}`
      //   })
      //   delete userData.setting
      // }


      // if (userData && userData.profile && userData.profile.image) {
      //   await db.updateSingle({
      //     Model: Models.UserPhoto,
      //     ModelData: userData.profile.image,
      //     searchQuery: {
      //       query: {
      //         _id: userData.profile.image._id
      //       }
      //     },
      //     cacheKey: `photo-id-${profile.image._id}`
      //   })
      //   delete userData.profile.image
      // }

      // if (userData && userData.profile) {
      //   await db.updateSingle({
      //     Model: Models.UserProfile,
      //     ModelData: userData.profile,
      //     searchQuery: {
      //       query: {
      //         _id: userData.profile._id
      //       }
      //     },
      //     cacheKey: `profile-id-${userData.profile._id}`
      //   })
      //   delete userData.profile
      // }

      return await db.updateSingle({
        Model: Models.User,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `user-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteUser (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.User,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `user-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }
}

const User = new UserResolver()

export const Resolvers = {
  Query: {
    User: User.getUser,
    Users: User.getUsers
  },
  Mutation: {
    createUser: User.createUser,
    updateUser: User.updateUser,
    deleteUser: User.deleteUser
  }
}