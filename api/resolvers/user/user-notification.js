class UserNotificationResolver {
  constructor () {

  }

  async getUserNotification (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getSingle({
        Model: Models.UserNotification,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `notification-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getUserNotifications (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.getCollection({
        Model: Models.UserNotification,
        searchQuery,
        cacheKey: `notifications-by-page-${JSON.stringify(searchQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createUserNotification (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.createSingle({
        Model: Models.UserNotification,
        ModelData: ModelData.input
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updateUserNotification (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.updateSingle({
        Model: Models.UserNotification,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `notification-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteUserNotification (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.deleteSingle({
        Model: Models.UserNotification,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `notification-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }
}

const UserNotification = new UserNotificationResolver()

export const Resolvers = {
  Query: {
    Notification: UserNotification.getUserNotification,
    Notifications: UserNotification.getUserNotifications
  }
}