class ProfileResolver {
  constructor () {

  }

  async getProfile (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null
      // console.log('getProfile', parent, searchQuery)
      if (parent) {
        DBQuery = {
          query: {
            $or:[{
              _id: (parent.profile) ? parent.profile : null 
            },{
              user: (parent.type && parent.user) ? parent.user : null
            }]
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getSingle({
        Model: Models.UserProfile,
        searchQuery: DBQuery,
        cacheKey: `profile-id-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async getProfiles (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let DBQuery = null

      if (parent) {
        const user = parent._id
        DBQuery = {
          query: {
            user: {
              $eq: user.toString(),
            }
          }
        }
      } else {
        DBQuery = searchQuery
      }    

      return await db.getCollection({
        Model: Models.UserProfile,
        searchQuery: DBQuery,
        cacheKey: `profiles-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async createProfile (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      let db = null
      db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.createSingle({
        Model: Models.UserProfile,
        ModelData: ModelData.input
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async updateProfile (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      let db = null
      db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.updateSingle({
        Model: Models.UserProfile,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `profile-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteProfile (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      
      return await db.deleteSingle({
        Model: Models.UserProfile,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `profile-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }


  async getFriendProfile (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null;

      if (parent) {
        DBQuery = {
          query: {
            user: {
              $in: parent.checked_in_with_friends
            }
          }
        }
      } else {
        DBQuery =  searchQuery
      }

      const { docs } = await db.getCollection({
        Model: Models.UserProfile,
        searchQuery: DBQuery,
        cacheKey: `profile-of-checked-in-friends-${JSON.stringify(DBQuery)}`
      })
      
      return docs
    } catch (e) {
      return new Error(e)
    }
  }
}

const Profile = new ProfileResolver()

export const Resolvers = {
  Query: {
    Profile: Profile.getProfile,
    Profiles: Profile.getProfiles
  },
  CheckIn: {
    checked_in_with_friends: Profile.getFriendProfile
  },
  Notification: {
    friend: Profile.getProfile,
    user: Profile.getProfile
  },
  User: {
    profile: Profile.getProfile
  },
  Mutation: {
    createProfile: Profile.createProfile,
    updateProfile: Profile.updateProfile,
    deleteProfile: Profile.deleteProfile
  }
}