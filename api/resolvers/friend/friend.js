import _ from 'lodash'
import moment from 'moment'
class FriendResolver {
  constructor () {

  }

  async getFriend (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null
      if (parent) {
        DBQuery = JSON.parse(JSON.stringify(_.merge(searchQuery, {
          query: {
            friend: parent.user
          }
        })))

        const data = await db.getSingle({
          Model: Models.Friend,
          searchQuery: DBQuery,
          cacheKey: `friend-id-${JSON.stringify(DBQuery)}`
        })
        return data ? {
          _id: data.user,
          approved: data.approved
        } : null

      } else {
        DBQuery = searchQuery

        return await db.getSingle({
          Model: Models.Friend,
          searchQuery: DBQuery,
          cacheKey: `friend-id-${JSON.stringify(DBQuery)}`
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async getFollowers (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null
      
      if (parent) {
        DBQuery = {
          query: {
            approved: true,
            friend: parent.user
          }
        }
    
        const data = await db.getCollection({
          Model: Models.Friend,
          searchQuery: DBQuery,
          cacheKey: `friend-by-page-${JSON.stringify(DBQuery)}`
        })

        const updatedDocs = _.map(data.docs, (user) => {
          return {
            _id: user.user
          }
        })

        return _.merge(data, {
          docs: updatedDocs
        })
      } else {
        DBQuery = searchQuery

        return await db.getCollection({
          Model: Models.Friend,
          searchQuery: DBQuery,
          cacheKey: `friend-by-page-${JSON.stringify(DBQuery)}`
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async getFollowing (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      let DBQuery = null
      
      if (parent) {
        DBQuery = JSON.parse(JSON.stringify(_.merge(searchQuery, {
          query: {
            user: parent.user
          }
        })))

        const data = await db.getCollection({
          Model: Models.Friend,
          searchQuery: DBQuery,
          cacheKey: `friend-by-page-${JSON.stringify(DBQuery)}`
        })
        
        const updatedDocs = _.map(data.docs, (user) => {
          return {
            _id: user.friend
          }
        })

        return _.merge(data, {
          docs: updatedDocs
        })
      } else {
        DBQuery = searchQuery

        return await db.getCollection({
          Model: Models.Friend,
          searchQuery: DBQuery,
          cacheKey: `friend-by-page-${JSON.stringify(DBQuery)}`
        })
      }
    } catch (e) {
      return new Error(e)
    }
  }

  async createFriend (parent, ModelData, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      const checkFriendQuery = {
        query: ModelData.input
      }

      const findFriend = await db.getSingle({
        Model: Models.Friend,
        searchQuery: checkFriendQuery,
        cacheKey: `friend-id-${JSON.stringify(checkFriendQuery)}`
      })
      
      if (findFriend && findFriend._id) {
        return findFriend
      } else {
        const userQuery = {
          query: {
            user: ModelData.input.user
          }
        }
    
        const friendQuery = {
          query: {
            user: ModelData.input.friend
          }
        }
    
        const user = await db.getSingle({
          Model: Models.UserSetting,
          searchQuery: userQuery,
          cacheKey: `user-id-${JSON.stringify(userQuery)}`
        })

        const friend = await db.getSingle({
          Model: Models.UserSetting,
          searchQuery: friendQuery,
          cacheKey: `user-id-${JSON.stringify(friendQuery)}`
        })

        const friendCreate = await db.createSingle({
          Model: Models.Friend,
          ModelData: {
            user: ModelData.input.user,
            friend: ModelData.input.friend,
            approved: !friend.privacy
          } 
        })
    
        await db.createSingle({
          Model: Models.UserNotification,
          ModelData: {
            message: friend.privacy ? 'has requested to be added' : 'has added you',
            friend: user.user,
            user: friend.user,
            mode: friend.privacy ? 'request' : 'update',
            type: 'friend'
          }
        })
    
        await db.createSingle({
          Model: Models.UserNotification,
          ModelData: {
            message: 'You have added a friend',
            friend: friend.user,
            user: user.user,
            mode : 'update',
            type: 'friend'
          }
        })
        // Send Notification
    
        return friendCreate 
      }
    } catch (e) {
      throw new Error (e)
    }
  }

  async updateFriend (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      return await db.updateSingle({
        Model: Models.Friend,
        ModelData: searchQuery.query,
        searchQuery: {
          query: {
            _id: searchQuery._id
          }
        },
        cacheKey: `friend-id-${searchQuery._id}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

  async deleteFriend (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db
      const friendDelete = await db.deleteSingle({
        Model: Models.Friend,
        searchQuery,
        cacheKey: `friend-id-${searchQuery}`
      })

      if (friendDelete && friendDelete._id) {
        await db.deleteSingle({
          Model: Models.UserNotification,
          searchQuery: {
            friend: searchQuery.query.user,
            user: searchQuery.query.friend,
            type: 'friend'
          }
        })

        await db.deleteSingle({
          Model: Models.UserNotification,
          searchQuery: {
            friend: searchQuery.query.friend,
            user: searchQuery.query.user,
            type: 'friend'
          }
        })
      }

      return friendDelete
    } catch (e) {
      return new Error(e)
    }
  }


  async getCheckInsAggregate (parent, searchQuery, {
    DBCacheHelpers,
    Models,
    DbInfo
  }) {
    try {
      const db = new DBCacheHelpers()
      db.cache = DbInfo.cache
      db.db = DbInfo.db

      let DBQuery = null
      if (parent) {
        const place_id = parent.place_id
        DBQuery = {
          query: {
            place_id: {
              $eq: place_id.toString(),
            },
            updatedAt: {
              $gte: `${moment().startOf('day').toDate()}`,
              $lt: `${moment().endOf('day').toDate()}`
            }
          }
        }
      } else {
        DBQuery = searchQuery
      }

      return await db.getCollection({
        Model: Models.Friend,
        searchQuery: DBQuery,
        cacheKey: `friend-checkin-aggregate-by-page-${JSON.stringify(DBQuery)}`
      })
    } catch (e) {
      return new Error(e)
    }
  }

}

const Friend = new FriendResolver()

export const Resolvers = {
  Query: {
    Friend: Friend.getFriend,
    Following: Friend.getFollowing,
    Followers: Friend.getFollowers,
    FriendCheckIns: Friend.getCheckInsAggregate
  },
  Profile: {
    Friend: Friend.getFriend,
    Following: Friend.getFollowing,
    Followers: Friend.getFollowers
  },
  Mutation: {
    createFriend: Friend.createFriend,
    updateFriend: Friend.updateFriend,
    deleteFriend: Friend.deleteFriend
  }
}